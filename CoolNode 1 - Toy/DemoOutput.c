/****************************************************************************

**************************************************************************/

#include "WirelessProtocols/Console.h"
#include "ConfigApp.h"
#include "HardwareProfile.h"
#include "WirelessProtocols/LCDBlocking.h"
#include "WirelessProtocols/MCHP_API.h"
#include "DemoOutput.h"
#include "WirelessProtocols/NVM.h"
#define LOWBYTE(v)   ((unsigned char) (v))
#define HIGHBYTE(v)  ((unsigned char) (((unsigned int) (v)) >> 8))
   char  READING;
   char Reading[2];
   BYTE sendReading[2];
   BYTE BCDReading[2];
int total = 0;
#define COMMISSION          0x01
#define CONTROL_CODE        0x02


typedef union{
    unsigned char byte_value;
    struct  {
   unsigned int nibble1 : 4;
   unsigned int nibble2 : 4;
            }bits;
}Nibbler;
Nibbler Nibblebyte;

/*************************************************************************/
// the following two variable arrays are the data to be transmitted
// in this demo. They are bit map of English word "MiWi" and "DE"
// respectively.
/*************************************************************************/
ROM const BYTE MiWi[6][21] = 
{
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A}
    
};   

ROM const BYTE DE[6][11] = 
{
    {0xB2,0xB2,0xB2,0x20,0x20,0xB2,0xB2,0xB2,0xB2,0x0D,0x0A},
    {0xB2,0x20,0x20,0xB2,0x20,0xB2,0x20,0x20,0x20,0x0D,0x0A},
    {0xB2,0x20,0x20,0xB2,0x20,0xB2,0xB2,0xB2,0xB2,0x0D,0x0A},
    {0xB2,0x20,0x20,0xB2,0x20,0xB2,0x20,0x20,0x20,0x0D,0x0A},
    {0xB2,0xB2,0xB2,0x20,0x20,0xB2,0xB2,0xB2,0xB2,0x0D,0x0A},
    {0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x0D,0x0A}
}; 
    typedef struct
    {
        BYTE Node_ON_OFF;           //ControlinfoArray[0]
        BYTE Node_Type;             //1

        BYTE Broadcast_Partner;     //2

        BYTE Action_Time_Length;  // 3 ---------------in seconds

        WORD Node_Setpoint_High;  // 4 & 5
        BYTE Node_Action_High;      // 6

        WORD Node_Setpoint_Low;   //7 & 8
        BYTE Node_Action_Low; //  9            0-7 indicating how to handle the tripoint

        WORD Light_Setpoint_High; // 10 &11
        BYTE Light_Action_High;   // 12

        WORD Light_Setpoint_Low; //13 & 14
        BYTE Light_Action_Low; //15
        BYTE Node_Setpoint_High_Trip; // 16
        BYTE Node_Setpoint_Low_Trip; //17
        BYTE Dark_Sensor_Trip; //18
        BYTE TOO_Bright_Sensor_Trip;//19
        BYTE Light_Sensor_Silence; //20
        BYTE Node_Sensor_Silence; //21
        BYTE Motor_Instruction ; //22
        WORD Motor_Target;  //23 & 24
        

    } MESSAGE_COMMANDS;

    MESSAGE_COMMANDS rxControlinfo;         // structure to contain the instructions for Node opperation

    BYTE ControlinfoArray[18]={00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00};    //ControlinfoArray[]

void rxControlRoutine (void)
{
    BYTE inputChar;
    char inputCharagain;
    int i;
    BYTE c;

      inputChar = ConsoleGet();    // Character received thru the USART

              if(inputChar == 0x43 || inputChar == 0x53) // This should be a "C" Read  port thus far
              {
               if(inputChar == 0x53) // This should be an "S" meaning send me the readings from the device
               {
                   ControlinfoArray[16] = 0xFF;
               }
               if(inputChar == 0x43) // This should be an "S" meaning send me the readings from the device
               {
                   ControlinfoArray[16] = 0x00;// shut off the send bit
               }
                  rxControlinfo.Node_Type = PORTBbits.RB1;  // read the port pin Node's type will be 1 or 0 ON mostly or OFF
                  ControlinfoArray[1] = rxControlinfo.Node_Type;
                  
                  if (PORTBbits.RB2 ) // tell the node to sense the light sensor
                  {
                      if (PORTBbits.RB3 )
                      {
                      rxControlinfo.Light_Action_High = 1;
                      ControlinfoArray[12]= 1;
                          rxControlinfo.Node_Action_High = 0;
                      ControlinfoArray[6]= 0;
                      }
                      else
                      {
                      rxControlinfo.Light_Action_High = 0;
                     ControlinfoArray[12]= 0;
                 
                      }
                  if (PORTBbits.RB4)

                  {
                      rxControlinfo.Light_Action_Low = 1;
                    rxControlinfo.Node_Action_Low = 0;
                    ControlinfoArray[15]= 1;
                    ControlinfoArray[9]= 0;
                  }
                    else
                     {
                      rxControlinfo.Light_Action_Low = 0;
                      rxControlinfo.Node_Action_Low = 0;
                      ControlinfoArray[9]=0;
                      ControlinfoArray[15]= 0;
                     }

                  }
                  else
                  {
                  if (PORTBbits.RB3)
                   {
                    rxControlinfo.Node_Action_High = 1;
                    ControlinfoArray[6]= 1;
                   rxControlinfo.Light_Action_High = 0;
                     ControlinfoArray[12]= 0;
                   }
                      else
                      {
                      rxControlinfo.Node_Action_High = 0;
                      ControlinfoArray[6]= 0;
                    rxControlinfo.Light_Action_High = 0;
                     ControlinfoArray[12]= 0;
                      }

                  if (PORTBbits.RB4)

                  {
                    rxControlinfo.Light_Action_Low = 0;
                    rxControlinfo.Node_Action_Low = 1;
                    ControlinfoArray[15]= 0;
                    ControlinfoArray[9]= 1;
                  }
                    else
                     {
                      rxControlinfo.Light_Action_Low = 0;
                      rxControlinfo.Node_Action_Low = 0;
                      ControlinfoArray[9]=0;
                      ControlinfoArray[15]= 0;
                     }

                  }


                  if (PORTBbits.RB5 )  // sending a Broadcast partner to listen to
                    {

                        rxControlinfo.Broadcast_Partner = 0xFF;
                        ControlinfoArray[2]= 0xFF;
                    }
                    else
                    {
                        rxControlinfo.Broadcast_Partner = 0x00;
                        ControlinfoArray[2]= 0x00;
                    }



              }

               if(inputChar == 0x4F) // This should be an "O" meaning turn ON the device
               {
                   rxControlinfo.Node_ON_OFF = 0xAA;
                   ControlinfoArray[0]= 0xAA;
                   ControlinfoArray[16] = 0x00;// shut off the send bit
               }
               if(inputChar == 0x46) // This should be an "F" meaning turn OFF the device
               {
                   rxControlinfo.Node_ON_OFF = 0xFF;
                   ControlinfoArray[0] = 0xFF;
                   ControlinfoArray[16] = 0x00;// shut off the send bit
               }
      
      /*This is the motor control */
                if(inputChar == 0x49) // This should drive the motor full reverse
               {
                   ControlinfoArray[17] = 0x00;
               }
                if(inputChar == 0x4A) // This should drive the motor  1/4 the way
               {
                   ControlinfoArray[17] = 0x01;   
               }
               if(inputChar == 0x4B) // This should drive the motor 1/2 the way
               {
                   ControlinfoArray[17] = 0x02; 
               }
               if(inputChar == 0x4C) // This should drive the motor  3/4
               {
                   ControlinfoArray[17] = 0x03;
               }
               if(inputChar == 0x4D) // This should drive the motor
               {
                   ControlinfoArray[17] = 0x04;
               }
               if(inputChar == 0x4E) // This should tell the motor to listen to the light
               {
                   ControlinfoArray[17] = 0x05;
               }




       Printf("\r\n");
       ConsolePutROMString((ROM char *)"Node Type :");
       PrintChar(rxControlinfo.Node_Type);
        Printf("\r\n");

       ConsolePutROMString((ROM char *)" Light Sensor Dark Output Action :");
       PrintChar(rxControlinfo.Light_Action_High);
        Printf("\r\n");
       ConsolePutROMString((ROM char *)" Light Sensor Too Bright Output Action :");
       PrintChar(rxControlinfo.Light_Action_Low);
        Printf("\r\n");
       ConsolePutROMString((ROM char *)" Node Sensor High Trip Point Output Action :");
       PrintChar(rxControlinfo.Node_Action_High);
         Printf("\r\n");
       ConsolePutROMString((ROM char *)" Node Sensor Low Trip Point Output Action :");
       PrintChar(rxControlinfo.Node_Action_Low);
        Printf("\r\n");
        Printf("\r\n");
        if(rxControlinfo.Broadcast_Partner)
        {
       ConsolePutROMString((ROM char *)" Node  Partner Mode ON :");Printf("\r\n Now PUSH NODE BUTTON TO PARTER WITH!");
        }
        else
        {
               ConsolePutROMString((ROM char *)" Node  Partner Mode OFF :");Printf("\r\n Now say GO !");
        }
         
      
 } 

void FlushData (void)

{
    int i;
/*   Data should be all set up, now transmit it out and cross your fingers.............. */

                    MiApp_FlushTx();
                    for(i = 0; i < 18; i++) // 18 bytes in each payload
                    {
                        MiApp_WriteData(ControlinfoArray[i]);
                        
                    }



}



void LastMinuiteDetails (void)

{
    BYTE inputChar;
    char inputCharagain;
    int i;

      inputChar = ConsoleGet();    // Character received thru the USART

              if (PORTAbits.RA6) // means Timer was set

              {
                  if(PORTBbits.RB1){
                      rxControlinfo.Action_Time_Length = 01;
                ControlinfoArray[3] = 01;}
                  if(PORTBbits.RB2){
                      rxControlinfo.Action_Time_Length = 02;
                ControlinfoArray[3] = 02;}
                  if(PORTBbits.RB3){
                      rxControlinfo.Action_Time_Length = 03;
                ControlinfoArray[3] = 03;}
                  if(PORTBbits.RB4){
                      rxControlinfo.Action_Time_Length = 04;
                ControlinfoArray[3] = 04;}
                  if(PORTBbits.RB5){
                      rxControlinfo.Action_Time_Length = 05;
                ControlinfoArray[3] = 05;}


              }
              else
              {
              rxControlinfo.Action_Time_Length = 0;
                ControlinfoArray[3] = 0;

              }




            switch (inputChar)
            {
                    case 0x31:
                      rxControlinfo.Broadcast_Partner = 0x01;
                      ControlinfoArray[2] = 0x01;
                           FlushData();

                        break;
                    case 0x32:
                      rxControlinfo.Broadcast_Partner = 0x02;
                      ControlinfoArray[2] = 0x02;
                           FlushData();
                        break;
                    case 0x33:
                      rxControlinfo.Broadcast_Partner = 0x03;
                      ControlinfoArray[2] = 0x03;
                           FlushData();
                         break;

                    case 0x34:
                      rxControlinfo.Broadcast_Partner = 0x04;
                      ControlinfoArray[2] = 0x04;
                           FlushData();
                         break;
                    case 0x35:
                      rxControlinfo.Broadcast_Partner = 0x05;
                      ControlinfoArray[2] = 0x05;
                           FlushData();
                         break;
                    case 0x36:
                      rxControlinfo.Broadcast_Partner = 0x06;
                      ControlinfoArray[2] = 0x06;
                           FlushData();
                         break;
                    case 0x37:
                      rxControlinfo.Broadcast_Partner = 0x07;
                      ControlinfoArray[2] = 0x07;
                           FlushData();
                         break;
                    case 0x38:
                      rxControlinfo.Broadcast_Partner = 0x08;
                      ControlinfoArray[2] = 0x08;
                           FlushData();
                         break;
                    case 0x39:
                      rxControlinfo.Broadcast_Partner = 0x09;
                      ControlinfoArray[2] = 0x09;
                           FlushData();
                         break;

                case 0x41:  // GO  either a Partner Button was pressed or just GO

     FlushData();
                    break;

            }
Printf("\r\n");
         ConsolePutROMString((ROM char *)" Node Partner  :");
       PrintChar(rxControlinfo.Broadcast_Partner);
         Printf("\r\n");
          Printf("\r\n");
                ConsolePutROMString((ROM char *)" Node Set Timer :");
                if(rxControlinfo.Action_Time_Length)
                {
                          Printf(" ON ");

         ConsolePutROMString((ROM char *)" for ");
                 switch(rxControlinfo.Action_Time_Length)
                 {
                                  case 01:
 ConsolePutROMString((ROM char *)"15 seconds");

                 break;

                                  case 02:
 ConsolePutROMString((ROM char *)"1 Minute");

                 break;
                                  case 03:
 ConsolePutROMString((ROM char *)"5 Minutes");

                 break;
                                  case 04:
 ConsolePutROMString((ROM char *)"1 hour");

                 break;
                                  case 05:
 ConsolePutROMString((ROM char *)"3 hours");

                 break;
                 default:
                     break;

                 }
                }
                else
                {
                          Printf(" OFF");
                }

         Printf("\r\n");
          Printf("\r\n");

}

void hextoDecimal (BYTE i)
{

    char c=0;
    int num = 0;
    int bcdtotal = 0;
        int digitOne;
int digitTwo;
int digitThree;
int digitFour ;
   Nibblebyte.byte_value = sendReading[i-1];
  c=Nibblebyte.bits.nibble1;
      if (c==0x00)num = 0;
    if (c==0x01)num = 1;
    if (c==0x02)num = 2;
    if (c==0x03)num = 3;
    if (c==0x04)num = 4;
    if (c==0x05)num = 5;
    if (c==0x06)num = 6;
    if (c==0x07)num = 7;
    if (c==0x08)num = 8;
    if (c==0x09)num = 9;
    if (c==0x0A)num = 10;
    if (c==0x0B)num = 11;
    if (c==0x0C)num = 12;
    if (c==0x0D)num = 13;
    if (c==0x0E)num = 14;
    if (c==0x0F)num = 15;
                  total = num;
   c=Nibblebyte.bits.nibble2;

    if (c==0x00)num = 0;
    if (c==0x01)num = 1;
    if (c==0x02)num = 2;
    if (c==0x03)num = 3;
    if (c==0x04)num = 4;
    if (c==0x05)num = 5;
    if (c==0x06)num = 6;
    if (c==0x07)num = 7;
    if (c==0x08)num = 8;
    if (c==0x09)num = 9;
    if (c==0x0A)num = 10;
    if (c==0x0B)num = 11;
    if (c==0x0C)num = 12;
    if (c==0x0D)num = 13;
    if (c==0x0E)num = 14;
    if (c==0x0F)num = 15;
                        num=num*16;
                    total = total + num;// This should be the running decimal total
        Nibblebyte.byte_value = sendReading[i];
                        c=Nibblebyte.bits.nibble1;
      if (c==0x00)num = 0;
    if (c==0x01)num = 1;
    if (c==0x02)num = 2;
    if (c==0x03)num = 3;
    if (c==0x04)num = 4;
    if (c==0x05)num = 5;
    if (c==0x06)num = 6;
    if (c==0x07)num = 7;
    if (c==0x08)num = 8;
    if (c==0x09)num = 9;
    if (c==0x0A)num = 10;
    if (c==0x0B)num = 11;
    if (c==0x0C)num = 12;
    if (c==0x0D)num = 13;
    if (c==0x0E)num = 14;
    if (c==0x0F)num = 15;
                        num=num*256;
                    total = total + num;
     digitOne = total / 1000;
 digitTwo = (total - digitOne * 1000) / 100;
 digitThree = (total - digitOne * 1000 - digitTwo * 100) / 10;
 digitFour = total - digitOne * 1000 - digitTwo * 100 - digitThree * 10;

BCDReading[1] = ( digitOne << 4 | digitTwo );
BCDReading[0] = (digitThree << 4 | digitFour);



}




void DemoOutput_HandleMessage(void)
{
    BYTE i;
    BYTE *table;
    BYTE a;
    BYTE writeaddr = 0x00;
    BYTE writecount = 9;
    BYTE PayloadIndex;

    if( rxMessage.flags.bits.broadcast == 1 )  // Broadcast from another node is detected
    {
                            switch( rxMessage.PayloadSize )
                            {

                            case 0x13:// means the HIGH SET POINT was tripped by the partner device

                                Blink();

                                 break;
                            case 0x12:// means a LOW SET POINT was tripped by the partner device

				 Blink();

                                break;
                            case 14:// means the DARK SET POINT was tripped by the partner device

                                Blink();

                                 break;

                            case 17:// means a BRIGHT SET POINT was tripped by the partner device

                                Blink();

                                break;

                            case 21:// means the MOTION was tripped by the partner device

				 Blink();

                                 break;
                            default:  // Sensor has sent reading somewhere so...........

                            break;
                    }
    }

    // otherwise it's a unicast from the PAN Coordinator

   if( rxMessage.flags.bits.broadcast == 0 )    //meaning unicast
        {
      switch( rxMessage.PayloadSize )  // this is the COMMISSION command from the server
        {
          case 05: // identify node
              Blink(); // Blink to indicate connection established
              break;


          case 07:
          PayloadIndex = 4;// pointing to the byte containing the pan id data
                            switch( rxMessage.Payload[0] )
                    {
                        case COMMISSION:
                            {
                                switch( rxMessage.Payload[1] )
                                {
                                    case 0x01:  // flash light

                                            Blink(); // Blink to indicate connection established

                                        break;

                                    case 0x02:  // set channel and PANID
                                    {
                                            WORD tmp = 0xFFFF;
                                            currentChannel = rxMessage.Payload[PayloadIndex];
                                            MiApp_SetChannel(currentChannel);

                                        	myPANID.v[0] = rxMessage.Payload[PayloadIndex+1];
                                        	myPANID.v[1] = rxMessage.Payload[PayloadIndex+2];
                                            MiMAC_SetAltAddress((BYTE *)&tmp, (BYTE *)&myPANID.Val);
                                        	nvmPutMyPANID(myPANID.v);
                                                 Blink(); // Blink to indicate connection established
                                    }
                                        break;
                                    default:
                                        break;
                                }
                          }
                   }
                break;

          default:
            break;
        }

        }

   }




void DemoOutput_UpdateTxRx(BYTE TxNum, BYTE RxNum)
{
    LCDTRXCount(TxNum, RxNum);  
}    

void DemoOutput_ChannelError(BYTE channel)
{
    Printf("\r\nSelection of channel ");
    PrintDec(channel);
    Printf(" is not supported in current configuration.\r\n");
}    

void DemoOutput_UnicastFail(void)
{
    Printf("\r\nUnicast Failed\r\n");                  
    LCDDisplay((char *)" Unicast Failed", 0, TRUE);
}    

void DemoOutput_Channel(BYTE channel, BYTE Step)
{
    if( Step == 0 )
    {
        LCDDisplay((char *)"Connecting Peer  on Channel %d ", channel, TRUE);
        Printf("\r\nConnecting Peer on Channel ");
        PrintDec(channel);
        Printf("\r\n");
    }
    else
    {
        LCDDisplay((char *)" Connected Peer  on Channel %d", channel, TRUE);
        Printf("\r\nConnected Peer on Channel ");
        PrintDec(channel);
        Printf("\r\n");
    }
}
