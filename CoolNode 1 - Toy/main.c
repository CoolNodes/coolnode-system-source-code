

/************************ HEADERS ****************************************/
#include "ConfigApp.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/Console.h"
#include "DemoOutput.h"
#include "HardwareProfile.h"
    #include "GenericTypeDefs.h"

/************************** VARIABLES ************************************/
#define LIGHT   0x01
#define MOTIONSENSOR  0x02
#define THERMOSTAT  0x03
#define SWITCH  0x04
BYTE num_nodes = 0;
BYTE Coolnodeindex = 0;
BYTE Poo; // keeps hold of payload size to separate Node types
BYTE CoolAddress[3];
BYTE EEPromData[9] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09};// 9 bytes of info to save and recover
extern BYTE     myLongAddress[];
extern WORD TimerOn;
extern BOOL FlipWriteBit;

MIWI_TICK tick1, tick2;
BYTE    connectionAddress[3];           // Directly connected to ?? (Remote Node address )
extern BYTE     myLongAddress[];
typedef struct
	{
	BYTE Address[2];
	}nodeIDs;

nodeIDs CoolNode[10]; //structure for each node's address

    BYTE *DestAddress;

/*************************************************************************/
// AdditionalNodeID variable array defines the additional 
// information to identify a device on a PAN. This array
// will be transmitted when initiate the connection between 
// the two devices. This  variable array will be stored in 
// the Connection Entry structure of the partner device. The 
// size of this array is ADDITIONAL_NODE_ID_SIZE, defined in 
// ConfigApp.h.
// In this demo, this variable array is set to be empty.
/*************************************************************************/
#if ADDITIONAL_NODE_ID_SIZE > 0
    BYTE AdditionalNodeID[ADDITIONAL_NODE_ID_SIZE] = {LIGHT};
#endif

/*************************************************************************/
// The variable myChannel defines the channel that the device
// is operate on. This variable will be only effective if energy scan
// (ENABLE_ED_SCAN) is not turned on. Once the energy scan is turned
// on, the operating channel will be one of the channels available with
// least amount of energy (or noise).
/*************************************************************************/
BYTE myChannel = 11;

BYTE CoolNodeID[8] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA};
extern DWORD_VAL OutgoingFrameCounter; 

/*********************************************************************
* Function:         void main(void)
		    
**********************************************************************/
#if defined(__18CXX)
void main(void)
#else
int main(void)
#endif
{   
   BYTE i;
    BYTE Send;
    BYTE TxSynCount = 0;
    BYTE TxSynCount2 = 0;
    BYTE TxNum = 0;
    BYTE RxNum = 0;
    BYTE addr = 0xFA;  //beginning address where the EEPROM is read from
    BYTE writeaddr = 0x50;
    BYTE count = 8;
   BYTE *DestAddress;
   BYTE *EEPromAddress0;
   BYTE *EEPromAddress8;
   BOOL NVMEnable = FALSE;
EEPromAddress0 = &EEPromData[0]; // this is the info that goes into the rxControlinfo
EEPromAddress8 = &EEPromData[8];
DestAddress = &myLongAddress[7];  // the pointer to the array gets passed to the EEPROMRead and decremented
    /*******************************************************************/
    // Initialize the system
    /*******************************************************************/
//  EEPROMRead(DestAddress,  addr,  count);  // reads the MAC LONG ADDRESS 8 BYTES
    BoardInit();// Sets up the inputs and outputs

// EEPROMRead(EEPromAddress8,  writeaddr,  9); // Read in the rxControlinfo
 ConsoleInit(); // This sets up the UART
   
    
    LED_1 = 0;
    LED_2 = 0;
 
               if(BUZZER == 0)// push button for initializing
        {
            MiApp_ProtocolInit(FALSE); // Initialize the network from scratch
            MiApp_SetChannel(myChannel) ;

        }
            else
            {
            NVMEnable = TRUE;
            MiApp_ProtocolInit(TRUE); // Initialize the network from NVM

            }



    /*******************************************************************/
    // Function MiApp_ConnectionMode defines the connection mode. The
    // possible connection modes are:
    //  ENABLE_ALL_CONN:    Enable all kinds of connection
    //  ENABLE_PREV_CONN:   Only allow connection already exists in 
    //                      connection table
    //  ENABL_ACTIVE_SCAN_RSP:  Allow response to Active scan
    //  DISABLE_ALL_CONN:   Disable all connections. 
    /*******************************************************************/
 MiApp_ConnectionMode(ENABLE_ACTIVE_SCAN_RSP  );



  
    Blink(); // Blink to indicate connection established
   

    while(1)
      
        
    {
        /*******************************************************************/
        // Function MiApp_MessageAvailable returns a boolean to indicate if 
        // a packet has been received by the transceiver. If a packet has 
        // been received, all information will be stored in the rxFrame, 
        // structure of RECEIVED_MESSAGE.
        /*******************************************************************/
        if( MiApp_MessageAvailable() )
        {
            
           
            
        
            DemoOutput_HandleMessage();
      
            MiApp_DiscardMessage();

                  
                        Blink();
                    
          }
        
        
    }
}
