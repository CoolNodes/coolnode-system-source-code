

/************************ HEADERS ****************************************/
#include "ConfigApp.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/Console.h"
#include "DemoOutput.h"
#include "HardwareProfile.h"
    #include "GenericTypeDefs.h"

/************************** VARIABLES ************************************/
#define LIGHT   0x01
#define MOTIONSENSOR  0x02
#define THERMOSTAT  0x03
#define SWITCH  0x04
BYTE num_nodes = 0;
BYTE Coolnodeindex = 0;
BYTE Poo; // keeps hold of payload size to separate Node types
BYTE CoolAddress[3];

BYTE    connectionAddress[3];           // Directly connected to ?? (Remote Node address )
extern BYTE     myLongAddress[];
typedef struct
	{
	BYTE Address[2];
	}nodeIDs;

nodeIDs CoolNode[10]; //structure for each node's address

    BYTE *DestAddress;

/*************************************************************************/
// AdditionalNodeID variable array defines the additional 
// information to identify a device on a PAN. This array
// will be transmitted when initiate the connection between 
// the two devices. This  variable array will be stored in 
// the Connection Entry structure of the partner device. The 
// size of this array is ADDITIONAL_NODE_ID_SIZE, defined in 
// ConfigApp.h.
// In this demo, this variable array is set to be empty.
/*************************************************************************/
#if ADDITIONAL_NODE_ID_SIZE > 0
    BYTE AdditionalNodeID[ADDITIONAL_NODE_ID_SIZE] = {LIGHT};
#endif

/*************************************************************************/
// The variable myChannel defines the channel that the device
// is operate on. This variable will be only effective if energy scan
// (ENABLE_ED_SCAN) is not turned on. Once the energy scan is turned
// on, the operating channel will be one of the channels available with
// least amount of energy (or noise).
/*************************************************************************/
BYTE myChannel = 24;

BYTE CoolNodeID[8] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA};
extern DWORD_VAL OutgoingFrameCounter; 

/*********************************************************************
* Function:         void main(void)
		    
**********************************************************************/
#if defined(__18CXX)
void main(void)
#else
int main(void)
#endif
{   
    BYTE i;
    BYTE TxSynCount = 0;
    BYTE TxSynCount2 = 0;
    BYTE TxNum = 0;
    BYTE RxNum = 0;
    BYTE    inputChar;

        BYTE addr = 0xFA;  //beginning address where the EEPROM is read from
    BYTE count = 8;
   
   BYTE  CoolnodeLongAddress[8];
   DestAddress = &myLongAddress[7];  // the pointer to the array gets passed to the EEPROMRead and decremented
    
    /*******************************************************************/
    // Initialize the system
    /*******************************************************************/
    BoardInit();
     EEPROMRead(DestAddress,  addr,  count);
     
     
    ConsoleInit(); // Important to initialize and send data thru RX TX
   
    
    LED_1 = 0;
    LED_2 = 0;
    BUZZER = 0;
   
// initialize the Coolnode array
    for (i=0; i<10;i++)
    {
    CoolNode[i].Address[1] = 0;
    CoolNode[i].Address[0] = 0;

    }

    /*******************************************************************/
    // Initialize Microchip proprietary protocol. Which protocol to use
    // depends on the configuration in ConfigApp.h
    /*******************************************************************/
    /*******************************************************************/
    // Function MiApp_ProtocolInit initialize the protocol stack. The
    // only input parameter indicates if previous network configuration
    // should be restored. In this simple example, we assume that the 
    // network starts from scratch.
    /*******************************************************************/
    MiApp_ProtocolInit(FALSE);
   
    // Set default channel
     MiApp_SetChannel(myChannel);

    /*******************************************************************/
    // Function MiApp_ConnectionMode defines the connection mode. The
    // possible connection modes are:
    //  ENABLE_ALL_CONN:    Enable all kinds of connection
    //  ENABLE_PREV_CONN:   Only allow connection already exists in 
    //                      connection table
    //  ENABL_ACTIVE_SCAN_RSP:  Allow response to Active scan
    //  DISABLE_ALL_CONN:   Disable all connections. 
    /*******************************************************************/
    // num_nodes = MiApp_SearchConnection(10, myChannel);
     MiApp_ConnectionMode(ENABLE_ALL_CONN );//
    DemoOutput_Channel(myChannel, 0);



    /*******************************************************************/
    // Function MiApp_EstablishConnection try to establish a new 
    // connection with peer device. 
    // The first parameter is the index to the active scan result, 
    //      which is acquired by discovery process (active scan). If 
    //      the value of the index is 0xFF, try to establish a 
    //      connection with any peer.
    // The second parameter is the mode to establish connection, 
    //      either direct or indirect. Direct mode means connection 
    //      within the radio range; indirect mode means connection 
    //      may or may not in the radio range. 
    /*******************************************************************/
   i = MiApp_EstablishConnection(0xFF, CONN_MODE_DIRECT);
    /*******************************************************************/
    // Display current opertion on LCD of demo board, if applicable


    // if( i != 0xFF )
   // {
    //Printf("\r\nConnecting Peer on Channel ");
    //    DemoOutput_Channel(myChannel, 1);
    //} //
    //else
   {
       
        /*******************************************************************/
        // If no network can be found and join, we need to start a new 
        // network by calling function MiApp_StartConnection
        //
        // The first parameter is the mode of start connection. There are 
        // two valid connection modes:
        //   - START_CONN_DIRECT        start the connection on current 
        //                              channel
        //   - START_CONN_ENERGY_SCN    perform an energy scan first, 
        //                              before starting the connection on 
        //                              the channel with least noise
        //   - START_CONN_CS_SCN        perform a carrier sense scan 
        //                              first, before starting the 
        //                              connection on the channel with 
        //                              least carrier sense noise. Not
        //                              supported for current radios
        //
        // The second parameter is the scan duration, which has the same 
        //     definition in Energy Scan. 10 is roughly 1 second. 9 is a 
        //     half second and 11 is 2 seconds. Maximum scan duration is 
        //     14, or roughly 16 seconds.
        //
        // The third parameter is the channel map. Bit 0 of the 
        //     double word parameter represents channel 0. For the 2.4GHz 
        //     frequency band, all possible channels are channel 11 to 
        //     channel 26. As the result, the bit map is 0x07FFF800. Stack 
        //     will filter out all invalid channels, so the application 
        //     only needs to pay attention to the channels that are not 
        //     preferred.
        /*******************************************************************/
        MiApp_StartConnection(START_CONN_DIRECT, 10, 0);//
    }
     //  
    

    /*******************************************************************/
    // Function DumpConnection is used to print out the content of the
    //  Connection Entry on the hyperterminal. It may be useful in 
    //  the debugging phase.
    // The only parameter of this function is the index of the  
    //  Connection Entry. The value of 0xFF means to print out all
    //  valid Connection Entry; otherwise, the Connection Entry
    //  of the input index will be printed out.
    /*******************************************************************/
    DumpConnection(0xFF);

  
    Blink(); // Blink to indicate connection established
   

    while(1)
      
        
    {
        /*******************************************************************/
        // Function MiApp_MessageAvailable returns a boolean to indicate if 
        // a packet has been received by the transceiver. If a packet has 
        // been received, all information will be stored in the rxFrame, 
        // structure of RECEIVED_MESSAGE.
        /*******************************************************************/
        if( MiApp_MessageAvailable() )
        {
            
            CoolNode[rxMessage.SourceAddress[0]].Address[0] = rxMessage.SourceAddress[0];
            
        
            DemoOutput_HandleMessage();
      
            MiApp_DiscardMessage();

                  
                        Blink();
                    
            }
        
        
        /* Right here we begin the interactive mode    and wait for instructions from the MCP2200  */
        
        
         
        
                   if(ConsoleIsGetReady())
            {
ConsolePutROMString((ROM char *)"\r\nSelect the Proper Grey Setup BUTTONS on Left and THEN Send Program Setup");
ProgramMode();
                 
  // call the rxControl routine
while(!ConsoleIsGetReady()){ Delay1KTCYx(200);}

rxControlRoutine();

while(!ConsoleIsGetReady()){ Delay1KTCYx(200);}

LastMinuiteDetails();


          
        MiApp_UnicastAddress(DestAddress, FALSE, FALSE);

         ConsolePutROMString((ROM char *)"Unicast Message Sent to: ");
         PrintChar( connectionAddress[1]);
            PrintChar( connectionAddress[0]);
          ConsolePutROMString((ROM char *)"\n\n");

 } 


            }
       }



void ProgramMode (void)
{
    BYTE inputChar;
            inputChar = ConsoleGet();    // Character received thru the USART

            switch (inputChar) //Nodes 1 - 9
            {
                    case 0x31:

            connectionAddress[1]=CoolNode[1].Address[1];
            connectionAddress[0]=CoolNode[1].Address[0];
            break;
                    case 0x32:

            connectionAddress[1]=CoolNode[2].Address[1];
            connectionAddress[0]=CoolNode[2].Address[0];
            break;
            case 0x33:

            connectionAddress[1]=CoolNode[3].Address[1];
            connectionAddress[0]=CoolNode[3].Address[0];
            break;

            case 0x34:

            connectionAddress[1]=CoolNode[4].Address[1];
            connectionAddress[0]=CoolNode[4].Address[0];
            break;

            case 0x35:

            connectionAddress[1]=CoolNode[5].Address[1];
            connectionAddress[0]=CoolNode[5].Address[0];
            break;

            case 0x36:

            connectionAddress[1]=CoolNode[6].Address[1];
            connectionAddress[0]=CoolNode[6].Address[0];
            break;

            case 0x37:

            connectionAddress[1]=CoolNode[7].Address[1];
            connectionAddress[0]=CoolNode[7].Address[0];
            break;

            case 0x38:

            connectionAddress[1]=CoolNode[8].Address[1];
            connectionAddress[0]=CoolNode[8].Address[0];
            break;

            case 0x39:

            connectionAddress[1]=CoolNode[9].Address[1];
            connectionAddress[0]=CoolNode[9].Address[0];
            break;

            }

            DestAddress = &connectionAddress[0];   // Now we know which node number we wanna address   nodes 1-9
}
