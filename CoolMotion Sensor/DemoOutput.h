/****************************************************************************
* FileName:		DemoOutput.h

**************************************************************************/


extern ROM const BYTE MiWi[6][21];
extern ROM const BYTE DE[6][11];


void DemoOutput_Greeting(void);
void DemoOutput_Channel(BYTE channel, BYTE step);
void DemoOutput_Instruction(void);
void DemoOutput_HandleMessage(void);
void DemoOutput_UpdateTxRx(BYTE TxNum, BYTE RxNum);
void DemoOutput_ChannelError(BYTE channel);
void DemoOutput_UnicastFail(void);


