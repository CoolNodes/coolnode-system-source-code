

/************************ HEADERS ****************************************/
#include "ConfigApp.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/Console.h"
#include "DemoOutput.h"
#include "HardwareProfile.h"
#include "WirelessProtocols/Console.h"
#include "ConfigApp.h"
#include "HardwareProfile.h"
#include "TimeDelay.h"

#include "WirelessProtocols/MSPI.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/SymbolTime.h"
#include "WirelessProtocols/NVM.h"
#include "Transceivers/Transceivers.h"

/************************ VARIABLES ********************************/
#define LIGHT   0x01
#define MOTIONSENSOR  0x02
#define THERMOSTAT  0x03
#define SWITCH  0x04
MIWI_TICK       tick1, tick2;
BYTE Coolnodeindex = 0;
BYTE Poo; // keeps hold of payload size to separate Node types
BYTE CoolAddress[3];

BYTE    connectionAddress[3];           // Directly connected to ?? (Remote Node address )

struct nodeIDs
	{
	BYTE Address[2]; //2 bytes
	char nodeType[12];//12 characters
        BYTE recognise;// not used
	};

struct nodeIDs CoolNode[10]; //10 structures for -each node's address
/*******************************************************************/
// AdditionalNodeID variable array defines the additional
// information to identify a device on a PAN. This array
// will be transmitted when initiate the connection between
// the two devices. This  variable array will be stored in
// the Connection Entry structure of the partner device. The
// size of this array is ADDITIONAL_NODE_ID_SIZE, defined in
// ConfigApp.h.
// In this demo, this variable array is set to be empty.
/******************************************************************/
#if ADDITIONAL_NODE_ID_SIZE > 0
    BYTE AdditionalNodeID[ADDITIONAL_NODE_ID_SIZE] = {SWITCH};
#endif

/*******************************************************************/
// The variable myChannel defines the channel that the connection
// is operate on. This variable will be only effective if energy scan
// (ENABLE_ED_SCAN) is not turned on. Once the energy scan is turned
// on, the operating channel will be one of the channels available with
// least amount of energy (or noise).
/*******************************************************************/
BYTE myChannel = 11;


/*********************************************************************
* Function:         void main(void)
*
* PreCondition:     none
*
* Input:		    none
*
* Output:		    none
*
* Side Effects:	    none
*
* Overview:		    This is the main function that runs the simple
*                   example demo. The purpose of this example is to
*                   demonstrate the simple application programming
*                   interface for the MiWi(TM) Development
*                   Environment. By virtually total of less than 30
*                   lines of code, we can develop a complete
*                   application using MiApp interface. The
*                   application will first try to establish a
*                   link with another device and then process the
*                   received information as well as transmit its own
*                   information.
*                   MiWi(TM) DE also support a set of rich
*                   features. Example code FeatureExample will
*                   demonstrate how to implement the rich features
*                   through MiApp programming interfaces.
*
* Note:
**********************************************************************/
#if defined(__18CXX)
void main(void)
#else
int main(void)
#endif
{
    BYTE i;
    BYTE TxSynCount = 0;
    BYTE TxSynCount2 = 0;
    BYTE TxNum = 0;
    BYTE RxNum = 0;
    BYTE *DestAddress;
    /*******************************************************************/
    // Initialize the system
    /*******************************************************************/
   
    BoardInit();
    ConsoleInit();

    

//    LED_1 = 1;
    //LED_2 = 0;

    /*******************************************************************/
    // Initialize Microchip proprietary protocol. Which protocol to use
    // depends on the configuration in ConfigApp.h
    /*******************************************************************/
    /*******************************************************************/
    // Function MiApp_ProtocolInit initialize the protocol stack. The
    // only input parameter indicates if previous network configuration
    // should be restored. In this simple example, we assume that the
    // network starts from scratch.
    /*******************************************************************/
    MiApp_ProtocolInit(FALSE);

    // Set default channel
   MiApp_SetChannel(myChannel);
   

    /*******************************************************************/
    // Function MiApp_ConnectionMode defines the connection mode. The
    // possible connection modes are:
    //  ENABLE_ALL_CONN:    Enable all kinds of connection
    //  ENABLE_PREV_CONN:   Only allow connection already exists in
    //                      connection table
    //  ENABL_ACTIVE_SCAN_RSP:  Allow response to Active scan
    //  DISABLE_ALL_CONN:   Disable all connections.
    /*******************************************************************/
MiApp_ConnectionMode(ENABLE_ACTIVE_SCAN_RSP);
    /*******************************************************************/
    // Function MiApp_EstablishConnection try to establish a new
    // connection with peer device.
    // The first parameter is the index to the active scan result, which
    //      is acquired by discovery process (active scan). If the value
    //      of the index is 0xFF, try to establish a connection with any
    //      peer.
    // The second parameter is the mode to establish connection, either
    //      direct or indirect. Direct mode means connection within the
    //      radio range; Indirect mode means connection may or may not
    //      in the radio range.
    /*******************************************************************/
//    if( (i = MiApp_EstablishConnection(0xFF, CONN_MODE_DIRECT)) == 0xFF );
    //DemoOutput_Channel(myChannel, 1);

    /*******************************************************************/
    // Function DumpConnection is used to print out the content of the
    //  Connection Entry on the hyperterminal. It may be useful in
    // the debugging phase.
    // The only parameter of this function is the index of the
    // Connection Entry. The value of 0xFF means to print out all
    //  valid Connection Entry; otherwise, the Connection Entry
    // of the input index will be printed out.
    /*******************************************************************/
    //DumpConnection(0xFF);

    // Blink LEDs to indicate connection established

    Blink(20); // Blink to indicate connection established
    //DemoOutput_Instruction();

    while(1)
    {
        /*******************************************************************/
        // Function MiApp_MessageAvailable returns a boolean to indicate if
        //  a packet has been received by the transceiver. If a packet has
        //  been received, all information will be stored in the rxFrame,
        //  structure of RECEIVED_MESSAGE.
        /*******************************************************************/
        if( MiApp_MessageAvailable() )
       {

            /*******************************************************************/
            // If a packet has been received, handle the information available
            // in rxMessage.
            /*******************************************************************/
            DemoOutput_HandleMessage();
            //DemoOutput_UpdateTxRx(TxNum, ++RxNum);

            // Toggle LED2 to indicate receiving a packet.
            //LED_2 ^= 1;
        	//CoolNode[1].Address[1] = rxMessage.Payload[1];
                //CoolNode[1].Address[0] = rxMessage.Payload[0];
            /*******************************************************************/
            // Function MiApp_DiscardMessage is used to release the current
            //  received packet.
            // After calling this function, the stack can start to process the
            //  next received frame
            /*******************************************************************/
            MiApp_DiscardMessage();

        }
        else
        {
            /*******************************************************************/
            // If no packet received, now we can check if we want to send out
            // any information.
            // Function ButtonPressed will return if any of the two buttons
            // pushed.
            /*******************************************************************/
            BYTE PressedButton = ButtonPressed();  // Motion sensed right here

            switch( PressedButton )
            {
                case 1:
                    /*******************************************************************/
                    // Button 1 pressed. We need to send out the bitmap of word "MiWi"
                    // First call MiApp_FlushTx to reset the Transmit buffer. Then fill
                    // the buffer one byte by one byte by calling function
                    // MiApp_WriteData
                    /*******************************************************************/
                    MiApp_FlushTx();
                    for(i = 0; i < 21; i++)
                    {
                        MiApp_WriteData(MiWi[(TxSynCount%6)][i]);
                    }
                    TxSynCount++;

                    /*******************************************************************/
                    // Function MiApp_BroadcastPacket is used to broadcast a message
                    //    The only parameter is the boolean to indicate if we need to
                    //       secure the frame
                    /*******************************************************************/


                    MiApp_BroadcastPacket(FALSE);
Blink(6);
                    /*******************************************************************/
                    // Here I check and see if the LED is Lit meaning it's supposed to
                    // send a Unicast message to the Coolnode who's address was sent by
                    // the PAN coordinator

              
                    break;


                default:
                    break;
                   
                    


            }

        
        } 
    }
}


