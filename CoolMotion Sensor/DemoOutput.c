/****************************************************************************
* FileName:		DemoOutput.c

**************************************************************************/

#include "WirelessProtocols/Console.h"
#include "ConfigApp.h"
#include "HardwareProfile.h"
#include "WirelessProtocols/LCDBlocking.h"
#include "WirelessProtocols/MCHP_API.h"

#define COMMISSION          0x01
#define CONTROL_CODE        0x02

/*************************************************************************/
// the following two variable arrays are the data to be transmitted
// in this demo. They are bit map of English word "MiWi" and "DE"
// respectively.
/*************************************************************************/
ROM const BYTE MiWi[6][21] = // Motion Detected
{
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A},
    {0x4d,0x6f,0x74,0x69,0x6f,0x6e,0x20,0x44,0x65,0x74,0x65,0x63,0x74,0x65,0x64,0x20,0x20,0x20,0x20,0x0D,0x0A}

};

ROM const BYTE DE[6][11] = 
{
    {0xB2,0xB2,0xB2,0x20,0x20,0xB2,0xB2,0xB2,0xB2,0x0D,0x0A},
    {0xB2,0x20,0x20,0xB2,0x20,0xB2,0x20,0x20,0x20,0x0D,0x0A},
    {0xB2,0x20,0x20,0xB2,0x20,0xB2,0xB2,0xB2,0xB2,0x0D,0x0A},
    {0xB2,0x20,0x20,0xB2,0x20,0xB2,0x20,0x20,0x20,0x0D,0x0A},
    {0xB2,0xB2,0xB2,0x20,0x20,0xB2,0xB2,0xB2,0xB2,0x0D,0x0A},
    {0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x0D,0x0A}
}; 

void DemoOutput_Greeting(void)
{
    #if defined(MRF49XA)
        #if defined(PROTOCOL_P2P)
            LCDDisplay((char *)"Simple P2P Demo  MRF49XA Node 2", 0, TRUE); 
        #endif
        #if defined(PROTOCOL_MIWI)
            LCDDisplay((char *)"Simple MiWi Demo MRF49XA Node 2", 0, TRUE); 
        #endif
        #if defined(PROTOCOL_MIWI_PRO)
            LCDDisplay((char *)"Simple MiWi PRO  MRF49XA Node 2", 0, TRUE); 
        #endif
    #elif defined(MRF24J40)
        #if defined(PROTOCOL_P2P)
            LCDDisplay((char *)"Simple P2P Demo MRF24J40 Node 2", 0, TRUE);
        #endif
        #if defined(PROTOCOL_MIWI)
            LCDDisplay((char *)"Simple MiWi DemoMRF24J40 Node 2", 0, TRUE);
        #endif
        #if defined(PROTOCOL_MIWI_PRO)
            LCDDisplay((char *)"Simple MiWi PRO MRF24J40 Node 2", 0, TRUE);
        #endif
    #elif defined(MRF89XA)
        #if defined(PROTOCOL_P2P)
            LCDDisplay((char *)"Simple P2P Demo  MRF89XA Node 2", 0, TRUE); 
        #endif
        #if defined(PROTOCOL_MIWI)
            LCDDisplay((char *)"Simple MiWi Demo MRF89XA Node 2", 0, TRUE); 
        #endif
        #if defined(PROTOCOL_MIWI_PRO)
            LCDDisplay((char *)"Simple MiWi PRO  MRF89XA Node 2", 0, TRUE); 
        #endif
    #endif

    #if defined(PROTOCOL_P2P)
        Printf("\r\nStarting Node 2 of Simple Demo for MiWi(TM) P2P Stack ...");  
    #endif
    #if defined(PROTOCOL_MIWI)
        Printf("\r\nStarting Node 2 of Simple Demo for MiWi(TM) Stack ...");  
    #endif 
    #if defined(PROTOCOL_MIWI_PRO)
        Printf("\r\nStarting Node 2 of Simple Demo for MiWi(TM) PRO Stack ...");  
    #endif 
    
    #if defined(PICDEMZ) 
        Printf("\r\nInput Configuration:");
        Printf("\r\n           Button 1: RB5");
        Printf("\r\n           Button 2: RB4");
        Printf("\r\nOutput Configuration:");
        Printf("\r\n                     RS232 port");
        Printf("\r\n              LED 1: RA0");
        Printf("\r\n              LED 2: RA1");
    #endif
    #if defined(PIC18_EXPLORER) 
        Printf("\r\nInput Configuration:");
        Printf("\r\n           Button 1: RB0");
        Printf("\r\n           Button 2: RA5");
        Printf("\r\nOutput Configuration:");
        Printf("\r\n                     RS232 port");
        Printf("\r\n                     USB port");
        Printf("\r\n              LED 1: D8");
        Printf("\r\n              LED 2: D7");
    #endif
    #if defined(EIGHT_BIT_WIRELESS_BOARD) 
        Printf("\r\nInput Configuration:");
        Printf("\r\n           Button 1: RB0");
        Printf("\r\n           Button 2: RB2");
        Printf("\r\nOutput Configuration:");
        Printf("\r\n              LED 1: RA2");
        Printf("\r\n              LED 2: RA3");
    #endif
    #if defined(EXPLORER16) 
        Printf("\r\nInput Configuration:");
        Printf("\r\n           Button 1: RD6");
        Printf("\r\n           Button 2: RD7");
        Printf("\r\nOutput Configuration:");
        Printf("\r\n                     RS232 port");
        Printf("\r\n              LED 1: D10");
        Printf("\r\n              LED 2: D9");
    #endif
    
    #if defined(MRF24J40)
    Printf("\r\n     RF Transceiver: MRF24J40");
    #elif defined(MRF49XA)
    Printf("\r\n     RF Transceiver: MRF49XA");
    #elif defined(MRF89XA)
    Printf("\r\n     RF Transceiver: MRF89XA");
    #endif
    Printf("\r\n   Demo Instruction:");
    Printf("\r\n                     Power on the board until LED 1 lights up");
    Printf("\r\n                     to indicate connecting with peer. Push");
    Printf("\r\n                     Button 1 to broadcast message. Push Button");
    Printf("\r\n                     2 to unicast encrypted message. LED 2 will");
    Printf("\r\n                     be toggled upon receiving messages. ");
    Printf("\r\n\r\n");    
    
}        

void DemoOutput_Channel(BYTE channel, BYTE Step)
{
    if( Step == 0 )
    {
        LCDDisplay((char *)"Connecting Peer  on Channel %d ", channel, TRUE);
        Printf("\r\nConnecting Peer on Channel ");
        PrintDec(channel);
        Printf("\r\n");
    }
    else
    {    
        LCDDisplay((char *)" Connected Peer  on Channel %d", channel, TRUE);
        Printf("\r\nConnected Peer on Channel ");
        PrintDec(channel);
        Printf("\r\n");
    }
}    

void DemoOutput_Instruction(void)
{
    #if defined(EXPLORER16)
        LCDDisplay((char *)"RD6: Broadcast  RD7: Unicast", 0, FALSE); 
    #elif defined(PIC18_EXPLORER)
        LCDDisplay((char *)"RB0: Broadcast  RA5: Unicast", 0, FALSE); 
    #elif defined(EIGHT_BIT_WIRELESS_BOARD)
        LCDDisplay((char *)"RB0: Broadcast  RB2: Unicast", 0, FALSE); 
    #endif
}    


void DemoOutput_HandleMessage(BYTE TxNum, BYTE RxNum)
{
    BYTE i;
    BYTE *table;
    BYTE a;
    BYTE writeaddr = 0x00;
    BYTE writecount = 9;
    BYTE PayloadIndex;



    // otherwise it's a unicast from the PAN Coordinator

   if( rxMessage.flags.bits.broadcast == 0 )    //meaning unicast
        {
      switch( rxMessage.PayloadSize )  // this is the COMMISSION command from the server
        {
          case 05: // identify node
              Blink(20); // Blink to indicate connection established
              break;


          case 07:
          PayloadIndex = 4;// pointing to the byte containing the pan id data
                            switch( rxMessage.Payload[0] )
                    {
                        case COMMISSION:
                            {
                                switch( rxMessage.Payload[1] )
                                {
                                    case 0x01:  // flash light

                                            Blink(20); // Blink to indicate connection established

                                        break;

                                    case 0x02:  // set channel and PANID
                                    {
                                            WORD tmp = 0xFFFF;
                                            currentChannel = rxMessage.Payload[PayloadIndex];
                                            MiApp_SetChannel(currentChannel);

                                        	myPANID.v[0] = rxMessage.Payload[PayloadIndex+1];
                                        	myPANID.v[1] = rxMessage.Payload[PayloadIndex+2];
                                            MiMAC_SetAltAddress((BYTE *)&tmp, (BYTE *)&myPANID.Val);
//                                        	nvmPutMyPANID(myPANID.v);
                                                 Blink(20); // Blink to indicate connection established
                                    }
                                        break;
                                    default:
                                        break;
                                }
                          }
                   }
                break;

          default:
            break;
        }

        }

   }

void DemoOutput_UpdateTxRx(BYTE TxNum, BYTE RxNum)
{
    LCDTRXCount(TxNum, RxNum);  
}    

void DemoOutput_ChannelError(BYTE channel)
{
    Printf("\r\nSelection of channel ");
    PrintDec(channel);
    Printf(" is not supported in current configuration.\r\n");
}    

void DemoOutput_UnicastFail(void)
{
    Printf("\r\nUnicast Failed\r\n");                  
    LCDDisplay((char *)" Unicast Failed", 0, TRUE);
}    

