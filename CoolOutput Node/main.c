/********************************************************************
* FileName:		main.c
* Dependencies: none
* Processor:	PIC18, PIC24, PIC32, dsPIC30, dsPIC33
*               tested with 18F4620, dsPIC33FJ256GP710
* Complier:     Microchip C18 v3.04 or higher
*				Microchip C30 v2.03 or higher
*               Microchip C32 v1.02 or higher
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaim
**************************************************************************/

/************************ HEADERS ****************************************/
#include "ConfigApp.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/Console.h"
#include "DemoOutput.h"
#include "HardwareProfile.h"
#include "ConfigApp.h"
#include "HardwareProfile.h"
#include "TimeDelay.h"
#include "WirelessProtocols/MSPI.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/SymbolTime.h"
#include "WirelessProtocols/NVM.h"
#include "Transceivers/Transceivers.h"
#include "WirelessProtocols/EEPROM.h"
#include "MAC_EEProm.h"

//#include "MAC_EEProm.h"

/************************ VARIABLES ********************************/
#define LIGHT   0x01
#define SWITCH  0x02
#define LOW 0x01
#define HIGH 0x02
#define OFF 0xFF
#define ON 0xAA
BYTE EEPromData[15] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F};// 15 bytes of info to save and recover
extern BYTE     myLongAddress[];
extern WORD TimerOn;
extern BOOL FlipWriteBit;
extern BOOL OverRideBit;

MIWI_TICK tick1, tick2;
/*******************************************************************/
// AdditionalNodeID variable array defines the additional
// information to identify a device on a PAN. This array
// will be transmitted when initiate the connection between
// the two devices. This  variable array will be stored in
// the Connection Entry structure of the partner device. The
// size of this array is ADDITIONAL_NODE_ID_SIZE, defined in
// ConfigApp.h.
// In this demo, this variable array is set to be empty.
/******************************************************************/
#if ADDITIONAL_NODE_ID_SIZE > 0
    BYTE AdditionalNodeID[ADDITIONAL_NODE_ID_SIZE] = {SWITCH};
#endif

/*******************************************************************/
// The variable myChannel defines the channel that the connection
// is operate on. This variable will be only effective if energy scan
// (ENABLE_ED_SCAN) is not turned on. Once the energy scan is turned
// on, the operating channel will be one of the channels available with
// least amount of energy (or noise).
/*******************************************************************/
BYTE myChannel = 11;


/*********************************************************************
* Function:         void main(void)
*
**********************************************************************/
#if defined(__18CXX)
void main(void)
#else
int main(void)
#endif
{
    BYTE i;
    BYTE Send;
    BYTE TxSynCount = 0;
    BYTE TxSynCount2 = 0;
    BYTE TxNum = 0;
    BYTE RxNum = 0;
    BYTE addr = 0xFA;  //beginning address where the EEPROM is read from
    BYTE writeaddr = 0x70;
    BYTE count = 8;
   BYTE *DestAddress;
   BYTE *EEPromAddress0;
   BYTE *EEPromAddress8;
   BOOL NVMEnable = FALSE;
EEPromAddress0 = &EEPromData[0]; 
EEPromAddress8 = &EEPromData[14];// this is the info that goes into the rxControlinfo
DestAddress = &myLongAddress[7];  // the pointer to the array gets passed to the EEPROMRead and decremented
    /*******************************************************************/
    // Initialize the system
    /*******************************************************************/

    BoardInit();// Sets up the inputs and outputs
  EEPROMRead(DestAddress,  addr,  count);  // reads the MAC LONG ADDRESS 8 BYTES
 EEPROMRead(EEPromAddress8,  writeaddr,  15); // Read in the rxControlinfo
 ConsoleInit(); // This sets up the UART
            if(PUSH_BUTTON_1 == 0)
        {
            MiApp_ProtocolInit(FALSE); // Initialize the network from scratch
            scratchInit();  // initialize the structure for Node commands from scratch
                RCSTA = 0x10; // Flip the SPEN bit to write
                EEPROMWrite(EEPromAddress8,  writeaddr,  15);
                 RCSTA = 0x90; // Flip back to enable UART
               MiApp_SetChannel(myChannel) ;    
                FlipWriteBit = FALSE;

        }
            else
            {
            NVMEnable = TRUE;
            MiApp_ProtocolInit(TRUE); // Initialize the network from NVM
            Control_init( ); // initialize the structure for Node commands
            }

    // Set default channel
       

        MiApp_ConnectionMode(ENABLE_ACTIVE_SCAN_RSP  );//ENABLE_ALL_CONN

        Blink(20); // Blink to indicate connection established

 
  
        CheckActionTime();
 while(1)
    {

     if (TimerOn !=00  )
     {
         tick2 = MiWi_TickGet();      // Get ready to time the outputs if that's the case
             if(MiWi_TickGetDiff(tick2, tick1) > (ONE_SECOND * TimerOn))
             {
                if(LED_1 == 1) // IN HERE YOU TEST THE LED AND IF ON - SHUT OFF OTHERWISE TURN ON

                {
                 Send = OFF;  // a variable to be the argument of the TimesUp function
                TimesUp(Send);
                 RCSTA = 0x10; // Flip the SPEN bit to write
                EEPROMWrite(EEPromAddress8,  writeaddr,  15);
                 RCSTA = 0x90; // Flip back to enable UART
                }
                else
                {
                 Send = ON;// a variable to be the argument of the TimesUp function
                 TimesUp(Send);
                RCSTA = 0x10; // Flip the SPEN bit to write
                EEPROMWrite(EEPromAddress8,  writeaddr,  15);
                 RCSTA = 0x90; // Flip back to enable UART
                }
                TimerOn=00;
              }
        }
     if (!OverRideBit)
     {
#ifdef DOUBLESENSORMODULE

ReadNode1Sensor();  // take humidity reading
#endif
#ifdef MOTORMODULE
MotorConrolRoutine (); // send commands to the motor
#endif
     /* read all sensors first and make a setpoint decision*/
ReadLightSensor();//internal Ambientlight sensor 

CompairLightRoutine();

ReadNodeSensor();// external Temperature and Soil Humidity sensor

CompairNodeSensorRoutine();
 
Check_Setpoints();

Check_Transmit();

     }

Check_OnOFF();   

                if(FlipWriteBit == TRUE)
            {
                 RCSTA = 0x10; // Flip the SPEN bit to write
                EEPROMWrite(EEPromAddress8,  writeaddr,  15);
                 RCSTA = 0x90; // Flip back to enable UART
                FlipWriteBit = FALSE;

            }

        /*******************************************************************/
        // Function MiApp_MessageAvailable returns a boolean to indicate if
        //  a packet has been received by the transceiver. If a packet has
        //  been received, all information will be stored in the rxFrame,
        //  structure of RECEIVED_MESSAGE.
        /*******************************************************************/
        if( MiApp_MessageAvailable() )
       {
            /*******************************************************************/
            // If a packet has been received, handle the information available
            // in rxMessage.
            /*******************************************************************/
            DemoOutput_HandleMessage();

            if(FlipWriteBit == TRUE)
            {
                 RCSTA = 0x10; // Flip the SPEN bit to write
                EEPROMWrite(EEPromAddress8,  writeaddr,  15);
                 RCSTA = 0x90; // Flip back to enable UART
                FlipWriteBit = FALSE;
                
            }


            MiApp_DiscardMessage();
        }
        else
        {
            /*******************************************************************/
            // If no packet received, now we can check if we want to send out
            // any information.
            // Function ButtonPressed will return if any of the two buttons
            // pushed.
            /*******************************************************************/
#ifdef MOTORMODULE
Check_Window ();  // this will broadcast when the window is OPEN and SHUT
# else 
            BYTE PressedButton = ButtonPressed();

            switch( PressedButton )
            {
                case 1:
                    /*******************************************************************/
                    // Button 1 pressed. We need to send out the bitmap of word "MiWi"
                    // First call MiApp_FlushTx to reset the Transmit buffer. Then fill
                    // the buffer one byte by one byte by calling function
                    // MiApp_WriteData
                    /*******************************************************************/
                    SendAllRoutine();

                    break;
                    case 2:

               


                    
                    SendAllRoutine();
                    
                    break;

                default:
                    break;
            }
#endif            
        }

    }
}

