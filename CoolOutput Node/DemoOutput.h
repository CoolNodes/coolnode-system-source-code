


extern ROM const BYTE MiWi[6][15];
extern ROM const BYTE DE[6][11];
extern ROM const BYTE SETPOINT_HIGH[6][19];//HERE WE SEPARATE THE PAYLOAD SIZES TO DECIDE THE OUTCOME
extern ROM const BYTE SETPOINT_LOW[6][18];
extern ROM const BYTE DARK_POINT[6][14];
extern ROM const BYTE TOO_BRIGHT[6][17];
extern ROM const BYTE WINDOW_SHUT[6][31];
extern ROM const BYTE WINDOW_OPEN[6][27];

void DemoOutput_Greeting(void);
void DemoOutput_Channel(BYTE channel, BYTE step);
void DemoOutput_Instruction(void);
void DemoOutput_HandleMessage(void);
void DemoOutput_UpdateTxRx(BYTE TxNum, BYTE RxNum);
void DemoOutput_ChannelError(BYTE channel);
void DemoOutput_UnicastFail(void);
void MotorConrolRoutine (void);
void ConfigADC_node(void);
void ConfigADC_node1(void);
void ConfigADC_node2(void);
void ConfigADC_node3(void);
void ReadNode1Sensor(void);
void ReadNode2Sensor(void);
void SendAllRoutine(void);
void ReadingAdjust(unsigned int Sensor);
void MotorConrolRoutine (void); // Open the window
void Check_Window (void);
void EEEPROMRead(BYTE *dest, BYTE addr, BYTE count);
void EEPROMWrite(BYTE *dest, BYTE addr, BYTE count);
void hextoDecimal(BYTE i);
    
