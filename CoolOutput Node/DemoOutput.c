/****************************************************************************
* FileName:		DemoOutput.c

**************************************************************************/

#include "WirelessProtocols/Console.h"
#include "ConfigApp.h"
 #include "delays.h"
#include "HardwareProfile.h"
#include "WirelessProtocols/LCDBlocking.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/NVM.h"
#include "WirelessProtocols/EEPROM.h"
#include "MAC_EEProm.h"
//#include "DemoOutput.h"
#define LOWBYTE(v)   ((unsigned char) (v))
#define HIGHBYTE(v)  ((unsigned char) (((unsigned int) (v)) >> 8))
#define COMMISSION          0x01
#define CONTROL_CODE        0x02
#define OPEN                0x01
#define CLOSED              0x02
#define humiditySensor      7
#define lightSensor         1    
#define waterlevelSensor    3
#define temperatureSensor   5
#define soilhumiditySensor  2
#define pressureSensor      6
#define switchSensor        4




BYTE serialBytes[4];
    
    BOOL SensorTrip = 0;
    BOOL WindowTrip = 0;
    BOOL LightTrip =0;
    BYTE tReading[2];
    BYTE actionTimeOld;
    WORD TimerOn = 00;
    BOOL FlipWriteBit = FALSE;
 
    BOOL OverRideBit = FALSE;
    BYTE WindowSetting = 0;

     WORD target = 00; // this is the Motor's target variable
     BYTE index = 0;
     
typedef union{
    unsigned char byte_value;
    struct  {
   unsigned int nibble1 : 4;
   unsigned int nibble2 : 4;
            }bits;
}Nibbler;
Nibbler Nibblebyte;
    BYTE sendReading[2];
    BYTE BCDReading[2];
    int total = 0;
#define LOWBYTE(v)   ((unsigned char) (v))
#define HIGHBYTE(v)  ((unsigned char) (((unsigned int) (v)) >> 8))
unsigned int  READING;       
     
 char Reading[18]; // the WORD gets converted into 2 bytes to be transmitted - 17 bytes of status to transmit
    #define samplingRate    5
WORD AmbientLight;            // Value of light being sensed
WORD NodeSensorReading;       // Value of the sensor on the Node
WORD ExternalLightReading;
WORD HumidityReading;
WORD RelativeHumidity;
long Temperature;
long Centigrade;
long Fahrenheit;
WORD MaxLength = 4095;
 extern   BYTE EEPromData[15];// 15 bytes of info to save and recover
   BYTE *EEPromAddress;

  
extern MIWI_TICK       tick1, tick2;
BYTE TxSynCount = 0;
      /***************************************************************************
     * Command Message information
     *
     *      This structure contains information about the received instructions
     *     from the program   *  added JGW
     **************************************************************************/
    typedef struct
    {
        BYTE Node_ON_OFF;           //ControlinfoArray[0]   received instruction   
        BYTE Node_Type;             //1    received instruction   

        BYTE Action_Time_Length;  // 2 ---------------in seconds    received instruction   

        WORD Node_Setpoint_High;  // 3 & 4
        
        BYTE Node_Action_High;      // 5     received instruction   

        WORD Node_Setpoint_Low;   //6 & 7
        
        BYTE Node_Action_Low; //  8           

        WORD Light_Setpoint_High; // 9 & 10
        
        BYTE Light_Action_High;   // 11

        WORD Light_Setpoint_Low; //12 & 13
        
        BYTE Light_Action_Low; //14
        
        BYTE Node_Setpoint_High_Trip; // 15
        
        BYTE Node_Setpoint_Low_Trip; //16
        
        BYTE Dark_Sensor_Trip; //17  internal light sensor high trip point
        
        BYTE TOO_Bright_Sensor_Trip;//18 internal light sensor low trip point
        
        BYTE Sensor_Type;    
        
        BYTE Motor_Instruction;

        WORD Motor_MaxLength;

    } MESSAGE_COMMANDS;

    MESSAGE_COMMANDS rxControlinfo;         // structure to contain the instructions for Node opperation

    WORD Temp;

BYTE SetPointsIn[4];// holds the status of the Node's 4 trip points  - Sensor High - Sensor Low - Dark - TOO Bright -  - these get transmitted

BYTE ReceiveinfoArray[8];    // the information is received into this array then distributed to the rxControlinfo structure
 WORD temp;
  WORD temp1;
 WORD temp2;
 WORD temp3;
 WORD temp4;
/*************************************************************************/
// the following two variable arrays are the data to be transmitted
// in this demo. They are bit map of English word "MiWi" and "DE"
// respectively.
/*************************************************************************/
ROM const BYTE MiWi[6][15] =
{
    {0x53,0x65,0x6E,0x73,0x6F,0x72,0x2F,0x53,0x77,0x69,0x74,0x63,0x68,0x0D,0x0A},
    {0x53,0x65,0x6E,0x73,0x6F,0x72,0x2F,0x53,0x77,0x69,0x74,0x63,0x68,0x0D,0x0A},
    {0x53,0x65,0x6E,0x73,0x6F,0x72,0x2F,0x53,0x77,0x69,0x74,0x63,0x68,0x0D,0x0A},
    {0x53,0x65,0x6E,0x73,0x6F,0x72,0x2F,0x53,0x77,0x69,0x74,0x63,0x68,0x0D,0x0A},
    {0x53,0x65,0x6E,0x73,0x6F,0x72,0x2F,0x53,0x77,0x69,0x74,0x63,0x68,0x0D,0x0A},
    {0x53,0x65,0x6E,0x73,0x6F,0x72,0x2F,0x53,0x77,0x69,0x74,0x63,0x68,0x0D,0x0A}

};
  
// Setpoint High Trip  
ROM const BYTE SETPOINT_HIGH[6][19] =
{
{0x53,0x65,0x6e,0x73,0x6f,0x72,0x20,0x48,0x61,0x73,0x20,0x54,0x72,0x69,0x70,0x70,0x65,0x64,0x0A},
{0x53,0x65,0x6e,0x73,0x6f,0x72,0x20,0x48,0x61,0x73,0x20,0x54,0x72,0x69,0x70,0x70,0x65,0x64,0x0A},
{0x53,0x65,0x6e,0x73,0x6f,0x72,0x20,0x48,0x61,0x73,0x20,0x54,0x72,0x69,0x70,0x70,0x65,0x64,0x0A},
{0x53,0x65,0x6e,0x73,0x6f,0x72,0x20,0x48,0x61,0x73,0x20,0x54,0x72,0x69,0x70,0x70,0x65,0x64,0x0A},
{0x53,0x65,0x6e,0x73,0x6f,0x72,0x20,0x48,0x61,0x73,0x20,0x54,0x72,0x69,0x70,0x70,0x65,0x64,0x0A},
{0x53,0x65,0x6e,0x73,0x6f,0x72,0x20,0x48,0x61,0x73,0x20,0x54,0x72,0x69,0x70,0x70,0x65,0x64,0x0A}
};


//Setpoint Low Trip
ROM const BYTE SETPOINT_LOW[6][18] =
{
{0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x20,0x4c,0x4f,0x57,0x20,0x54,0x72,0x69,0x70,0x0A},
{0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x20,0x4c,0x4f,0x57,0x20,0x54,0x72,0x69,0x70,0x0A},
{0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x20,0x4c,0x4f,0x57,0x20,0x54,0x72,0x69,0x70,0x0A},
{0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x20,0x4c,0x4f,0x57,0x20,0x54,0x72,0x69,0x70,0x0A},
{0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x20,0x4c,0x4f,0x57,0x20,0x54,0x72,0x69,0x70,0x0A},
 {0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x20,0x4c,0x4f,0x57,0x20,0x54,0x72,0x69,0x70,0x0A}
};

// DARK Setpoint
ROM const BYTE DARK_POINT[6][14] =
{
{0x44,0x41,0x52,0x4b,0x20,0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x0A},
{0x44,0x41,0x52,0x4b,0x20,0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x0A},
{0x44,0x41,0x52,0x4b,0x20,0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x0A},
{0x44,0x41,0x52,0x4b,0x20,0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x0A},
{0x44,0x41,0x52,0x4b,0x20,0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x0A},
{0x44,0x41,0x52,0x4b,0x20,0x53,0x65,0x74,0x70,0x6f,0x69,0x6e,0x74,0x0A}
};

// It's TOOO Bright
ROM const BYTE TOO_BRIGHT[6][17] =
{
{0x49,0x74,0x27,0x73,0x20,0x54,0x4f,0x4f,0x4f,0x20,0x42,0x72,0x69,0x67,0x68,0x74,0x0A},
{0x49,0x74,0x27,0x73,0x20,0x54,0x4f,0x4f,0x4f,0x20,0x42,0x72,0x69,0x67,0x68,0x74,0x0A},
{0x49,0x74,0x27,0x73,0x20,0x54,0x4f,0x4f,0x4f,0x20,0x42,0x72,0x69,0x67,0x68,0x74,0x0A},
{0x49,0x74,0x27,0x73,0x20,0x54,0x4f,0x4f,0x4f,0x20,0x42,0x72,0x69,0x67,0x68,0x74,0x0A},
{0x49,0x74,0x27,0x73,0x20,0x54,0x4f,0x4f,0x4f,0x20,0x42,0x72,0x69,0x67,0x68,0x74,0x0A},
{0x49,0x74,0x27,0x73,0x20,0x54,0x4f,0x4f,0x4f,0x20,0x42,0x72,0x69,0x67,0x68,0x74,0x0A}
};
//The Window has been Opened
ROM const BYTE WINDOW_OPEN[6][27] =
{
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x68,0x61,0x73,0x20,0x62,0x65,0x65,0x6e,0x20,0x4f,0x70,0x65,0x6e,0x65,0x64,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x68,0x61,0x73,0x20,0x62,0x65,0x65,0x6e,0x20,0x4f,0x70,0x65,0x6e,0x65,0x64,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x68,0x61,0x73,0x20,0x62,0x65,0x65,0x6e,0x20,0x4f,0x70,0x65,0x6e,0x65,0x64,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x68,0x61,0x73,0x20,0x62,0x65,0x65,0x6e,0x20,0x4f,0x70,0x65,0x6e,0x65,0x64,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x68,0x61,0x73,0x20,0x62,0x65,0x65,0x6e,0x20,0x4f,0x70,0x65,0x6e,0x65,0x64,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x68,0x61,0x73,0x20,0x62,0x65,0x65,0x6e,0x20,0x4f,0x70,0x65,0x6e,0x65,0x64,0x0A}
};
//The Window is now shut Tightly
ROM const BYTE WINDOW_SHUT[6][31] =
{
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x69,0x73,0x20,0x6e,0x6f,0x77,0x20,0x73,0x68,0x75,0x74,0x20,0x54,0x69,0x67,0x68,0x74,0x6c,0x79,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x69,0x73,0x20,0x6e,0x6f,0x77,0x20,0x73,0x68,0x75,0x74,0x20,0x54,0x69,0x67,0x68,0x74,0x6c,0x79,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x69,0x73,0x20,0x6e,0x6f,0x77,0x20,0x73,0x68,0x75,0x74,0x20,0x54,0x69,0x67,0x68,0x74,0x6c,0x79,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x69,0x73,0x20,0x6e,0x6f,0x77,0x20,0x73,0x68,0x75,0x74,0x20,0x54,0x69,0x67,0x68,0x74,0x6c,0x79,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x69,0x73,0x20,0x6e,0x6f,0x77,0x20,0x73,0x68,0x75,0x74,0x20,0x54,0x69,0x67,0x68,0x74,0x6c,0x79,0x0A},
{0x54,0x68,0x65,0x20,0x57,0x69,0x6e,0x64,0x6f,0x77,0x20,0x69,0x73,0x20,0x6e,0x6f,0x77,0x20,0x73,0x68,0x75,0x74,0x20,0x54,0x69,0x67,0x68,0x74,0x6c,0x79,0x0A}
};



void Control_init(void)  /*  Here we just pull all the data out of the EEProm once the node has been programmed and powered down then back up */
{


    rxControlinfo.Node_ON_OFF = EEPromData[0];
    rxControlinfo.Node_Type = EEPromData[1];
    rxControlinfo.Action_Time_Length = actionTimeOld = EEPromData[2];    
    rxControlinfo.Node_Action_High = EEPromData[3];// tells the node to react or not to the sensor trip
    rxControlinfo.Node_Action_Low = EEPromData[4];// tells the node to react or not to the sensor trip
    rxControlinfo.Light_Action_High = EEPromData[5];// tells the node to react or not to the sensor trip
    rxControlinfo.Light_Action_Low = EEPromData[6];// tells the node to react or not to the sensor trip
    rxControlinfo.Motor_Instruction = EEPromData[7];
    rxControlinfo.Sensor_Type = EEPromData[8];
    temp1 = EEPromData[10];temp2 = EEPromData[9];
    temp1 = (temp1 << 8);
    temp3 = (temp1 | temp2);
    temp1 = EEPromData[12];temp2 = EEPromData[11];
    temp1 = (temp1 << 8);
    temp4 = (temp1 | temp2);

        switch (rxControlinfo.Sensor_Type)
        {
            case 00:
    rxControlinfo.Light_Setpoint_High = 0 ;  // these are good setpoints for the other sensors
    rxControlinfo.Light_Setpoint_Low = 0;//
    rxControlinfo.Light_Setpoint_High = 0 ;  // these are good setpoints for the other sensors
    rxControlinfo.Light_Setpoint_Low = 0;//
                break;
            case 01:
      
    rxControlinfo.Light_Setpoint_High = temp3;  // these are good setpoints for the other sensors
  
    rxControlinfo.Light_Setpoint_Low = temp4;
                break;
            default:
  
    rxControlinfo.Node_Setpoint_High = temp3;  // these are good setpoints for the other sensors

    rxControlinfo.Node_Setpoint_Low = temp4;
                break;
        }


    rxControlinfo.Node_Setpoint_High_Trip = 0;
    rxControlinfo.Node_Setpoint_Low_Trip = 0;

    rxControlinfo.TOO_Bright_Sensor_Trip = 0;
    rxControlinfo.Dark_Sensor_Trip = 0;

    temp1 = EEPromData[14];temp2 = EEPromData[13];
    temp1 = (temp1 << 8);
    temp3 = (temp1 | temp2);
    rxControlinfo.Motor_MaxLength = MaxLength = temp3;
    
}

void scratchInit(void)  /* Here we are initializing BOTH the EEPromData string and the rxControlinfo structure */
{
    rxControlinfo.Node_ON_OFF = EEPromData[0] = 0xFF;
    rxControlinfo.Node_Type = EEPromData[1] = 00;
    rxControlinfo.Action_Time_Length = EEPromData[2] = actionTimeOld = 00;
    rxControlinfo.Node_Action_High = EEPromData[3] = 00;// tells the node to react or not to the sensor trip
    rxControlinfo.Node_Action_Low = EEPromData[4] = 00;// tells the node to react or not to the sensor trip
    rxControlinfo.Light_Action_High = EEPromData[5] = 00;// tells the node to react or not to the sensor trip
    rxControlinfo.Light_Action_Low = EEPromData[6] = 00;// tells the node to react or not to the sensor trip
    rxControlinfo.Motor_Instruction = EEPromData[7] = 00;
    rxControlinfo.Sensor_Type = EEPromData[8] = 01; // light sensor is the default
    
    rxControlinfo.Light_Setpoint_High  = 800;  // these are good setpoints for the other sensors
    ReadingAdjust( rxControlinfo.Light_Setpoint_High);
    EEPromData[9] = Reading[0];
    EEPromData[10]= Reading[1];
    rxControlinfo.Light_Setpoint_Low = 400; index = 2;
    ReadingAdjust( rxControlinfo.Light_Setpoint_Low);
    EEPromData[11] = Reading[2];
    EEPromData[12] = Reading[3];    
    rxControlinfo.Node_Setpoint_High = 800; 
     
    rxControlinfo.Node_Setpoint_Low = 400;
#ifdef  MOTORMODULE
    temp1 = EEPromData[14];temp2 = EEPromData[13];
    temp1 = (temp1 << 8);
    temp3 = (temp1 | temp2);
    rxControlinfo.Motor_MaxLength = MaxLength = temp3;
#endif
}
void Check_SetpointsSettings (void)
{
    switch (rxControlinfo.Sensor_Type)
    {
        case 00:
    rxControlinfo.Node_Setpoint_Low = 0;//   Switch like window open/close switch
    rxControlinfo.Node_Setpoint_High = 0;//
    rxControlinfo.Light_Setpoint_Low = 0;//   Switch like window open/close switch
    rxControlinfo.Light_Setpoint_High = 0;//
            break;
        case 01:
    rxControlinfo.Light_Setpoint_Low = 400;//   Switch like window open/close switch
    rxControlinfo.Light_Setpoint_High = 800;//

            break;
        case 02:
    rxControlinfo.Node_Setpoint_Low = 100;//
    rxControlinfo.Node_Setpoint_High = 300;// rough setpoints for soil sensing
            break;
        case 03:
    rxControlinfo.Node_Setpoint_Low = 400;//
    rxControlinfo.Node_Setpoint_High = 800;//rough water level sensing logic levels translated into HIGH and LOW
            break;
        case 04:
    rxControlinfo.Node_Setpoint_Low = 400;//   Switch like window open/close switch
    rxControlinfo.Node_Setpoint_High = 800;//
            break;
        case 05:
    rxControlinfo.Node_Setpoint_Low = 75;// degrees
    rxControlinfo.Node_Setpoint_High = 80;// degrees
            break;
        case 06:
    rxControlinfo.Node_Setpoint_Low = 400;//  pressure
    rxControlinfo.Node_Setpoint_High = 800;//
            break;
        case 07:
    rxControlinfo.Node_Setpoint_Low = 75;//  room humidity
    rxControlinfo.Node_Setpoint_High = 90;//
            break;
        default:
            break;
    }
}
void Check_OnOFF(void)
{ 
 if (rxControlinfo.Node_ON_OFF == 0xAA)  // Node_Type 0 is an OFF most of the time unit
    {
        LED_1 = 1;
        LED_2 = 1;     
    }
if (rxControlinfo.Node_ON_OFF == 0xFF)
    {
        LED_1 = 0;
        LED_2 = 0;      
    }
}

void TimesUp(BYTE onoff)
{
    BYTE accept;
    accept  =  onoff  ;

    EEPromData[0] = rxControlinfo.Node_ON_OFF = accept;
}

void Check_Setpoints (void)
{
    if(SensorTrip==0 || SensorTrip ==2)
    {
if(rxControlinfo.Node_Setpoint_High_Trip)
 {
  if(rxControlinfo.Node_Action_High == 1 )  
  {
  rxControlinfo.Node_ON_OFF = EEPromData[0] = 0xAA;
  
#if defined(MOTORMODULE)

  switch(rxControlinfo.Node_Type )
  {
      case 00:
      rxControlinfo.Motor_Instruction = EEPromData[7] = 0;// pull the actuator home to open a window
      LED_3 = 1;
      WindowSetting = OPEN;
          break;
      case 01:
      rxControlinfo.Motor_Instruction = EEPromData[7] = 4;// push the actuator out to open a vent
      LED_3 = 1;
      WindowSetting = OPEN;
          break;
  }          
#endif
  
Check_Node_Type();
    FlipWriteBit = TRUE;
 
  }
 }
}
if(SensorTrip==0 || SensorTrip ==2)
{
if(rxControlinfo.Node_Setpoint_Low_Trip )
{
  if(rxControlinfo.Node_Action_Low == 1)//if you're supposed to do anything
  {
   rxControlinfo.Node_ON_OFF = EEPromData[0] = 0xFF;
#if defined(MOTORMODULE)

  switch(rxControlinfo.Node_Type )
  {
      case 00:
      rxControlinfo.Motor_Instruction = EEPromData[7] = 4;// push the actuator out to open a vent
      LED_3 = 0;
      WindowSetting = CLOSED;
          break;
      case 01:
      rxControlinfo.Motor_Instruction = EEPromData[7] = 0;// pull the actuator home to open a window
      LED_3 = 0;
      WindowSetting = CLOSED;
          break;
  }
  

#endif
  FlipWriteBit = TRUE;
  Check_Node_Type();
}
}
}



if(LightTrip==0 || LightTrip ==2)
{
if(rxControlinfo.Dark_Sensor_Trip )//(rxControlinfo.TOO_Bright_Sensor_Trip)// with the internal light sensor the low is bright
//(rxControlinfo.TOO_Bright_Sensor_Trip)//(rxControlinfo.Dark_Sensor_Trip)external sensor   
{
    if(rxControlinfo.Light_Action_High == 1)
    {
  rxControlinfo.Node_ON_OFF = EEPromData[0] = 0xAA;
#if defined(MOTORMODULE)

  switch(rxControlinfo.Node_Type )
  {
      case 00:
      rxControlinfo.Motor_Instruction = EEPromData[7] = 4;// push the actuator home to open a window
      LED_3 = 0;
      WindowSetting = CLOSED;
          break;
      case 01:
      rxControlinfo.Motor_Instruction = EEPromData[7] = 0;//pull the actuator out to open a vent
      LED_3 = 0;
      WindowSetting = CLOSED;
          break;
  }


#endif 
   Check_Node_Type();
    FlipWriteBit = TRUE;
     
    }
}
}

if (AmbientLight < rxControlinfo.Light_Setpoint_High && AmbientLight > rxControlinfo.Light_Setpoint_Low)   ////////// Means IT's Normal Brightness
{
    if(rxControlinfo.Light_Action_Low == 1 || rxControlinfo.Light_Action_High == 1)
    {
  rxControlinfo.Node_ON_OFF = EEPromData[0] = 0xFF;
  LightTrip = 0;
  Check_Node_Type();
  #if defined(MOTORMODULE)
   rxControlinfo.Motor_Instruction = EEPromData[7] = 5;// set window to run with Sun
//  LED_3 = 0;  // here we'll turn oFF the 12Volt fan because we're under 73 degrees
  WindowSetting = OPEN;


#endif

    }
}

}
#if defined(MOTORMODULE)
void Check_Window (void)
{
    INT i;
    if(WindowTrip!=1)
    {
        if(Window_Switch == 1 )
        {
                    MiApp_FlushTx();
                    for(i = 0; i < 27; i++)
                    {
                    MiApp_WriteData(WINDOW_OPEN[(TxSynCount%6)][i]);
                    }
                    TxSynCount++;


                MiApp_BroadcastPacket(FALSE); // Broadcast The Window has been Opened

                tick1 = MiWi_TickGet(); tick2 = MiWi_TickGet();
              while (MiWi_TickGetDiff(tick2, tick1) < (ONE_SECOND /4)==TRUE) // Delay a second before moving on
              {
              tick2 = MiWi_TickGet();
              }
        WindowSetting = OPEN;
        WindowTrip = 1;
       
        }
    }
        if(WindowTrip==0 || WindowTrip ==1)
    {
        if(Window_Switch == 0 )
        {
                    MiApp_FlushTx();
                    for(i = 0; i < 31; i++)
                    {
                    MiApp_WriteData(WINDOW_SHUT[(TxSynCount%6)][i]);
                    }
                    TxSynCount++;


                MiApp_BroadcastPacket(FALSE); // Broadcast The Window is now shut Tightly

                tick1 = MiWi_TickGet(); tick2 = MiWi_TickGet();
              while (MiWi_TickGetDiff(tick2, tick1) < (ONE_SECOND /4)==TRUE) // Delay a second before moving on
              {
              tick2 = MiWi_TickGet();
              }
        WindowTrip = 2;
        WindowSetting = CLOSED;
        }
    }
} 
#endif

void Check_Transmit (void)
{
    int i;
    if(SensorTrip==0 || SensorTrip ==2)
    {
  if(rxControlinfo.Node_Setpoint_High_Trip && rxControlinfo.Node_Action_High)
  {
                    MiApp_FlushTx();
                    for(i = 0; i < 19; i++)
                    {
                        MiApp_WriteData(SETPOINT_HIGH[(TxSynCount%6)][i]);
                    }
                    TxSynCount++;
                    MiApp_BroadcastPacket(FALSE); // Broadcast the info for all nodes tuned in
                tick1 = MiWi_TickGet(); tick2 = MiWi_TickGet();

             while (MiWi_TickGetDiff(tick2, tick1) < (ONE_SECOND /4)==TRUE) // Delay a second before moving on
              {
              tick2 = MiWi_TickGet();
              }
    SensorTrip = 1;
  
   }
    }
    if ( SensorTrip ==0 ||  SensorTrip ==1)
    {
   if(rxControlinfo.Node_Setpoint_Low_Trip && rxControlinfo.Node_Action_Low)
   {
                    MiApp_FlushTx();
                    for(i = 0; i < 18; i++)
                    {
                         MiApp_WriteData(SETPOINT_LOW[(TxSynCount%6)][i]);
                    }
                    TxSynCount++;
                    MiApp_BroadcastPacket(FALSE); // Broadcast the info for all nodes tuned in
                  tick1 = MiWi_TickGet(); tick2 = MiWi_TickGet();

              while (MiWi_TickGetDiff(tick2, tick1) < (ONE_SECOND /4)==TRUE) // Delay a second before moving on
              {
              tick2 = MiWi_TickGet();
              }
  SensorTrip = 2;
  
   }
    }
   if(LightTrip==0 || LightTrip ==2)
   {
  if(rxControlinfo.Dark_Sensor_Trip &&  rxControlinfo.Light_Action_High)
    {
                    MiApp_FlushTx();
                    for(i = 0; i < 14; i++)
                    {
                         MiApp_WriteData(DARK_POINT[(TxSynCount%6)][i]);
                    }
                    TxSynCount++;

                    MiApp_BroadcastPacket(FALSE);
                                  tick1 = MiWi_TickGet(); tick2 = MiWi_TickGet();

              while (MiWi_TickGetDiff(tick2, tick1) < (ONE_SECOND /4)==TRUE) // Delay a quarter of a second before moving on
              {
              tick2 = MiWi_TickGet();
              }  
  LightTrip = 1;
  
   }
   }
    if ( LightTrip ==0 ||  LightTrip ==1)
    {
  if(rxControlinfo.TOO_Bright_Sensor_Trip  && rxControlinfo.Light_Action_High)
    {
                   MiApp_FlushTx();
                    for(i = 0; i < 17; i++)
                    {
                         MiApp_WriteData(TOO_BRIGHT[(TxSynCount%6)][i]);
                    }
                    TxSynCount++;


                    /*******************************************************************/
                    // Function MiApp_BroadcastPacket is used to broadcast a message
                    //    The only parameter is the boolean to indicate if we need to
                    //       secure the frame
                    /*******************************************************************/
    MiApp_BroadcastPacket(FALSE); // Broadcast the info for all nodes tuned in
                  tick1 = MiWi_TickGet(); tick2 = MiWi_TickGet();

              while (MiWi_TickGetDiff(tick2, tick1) < (ONE_SECOND /4)==TRUE) // Delay a second before moving on
              {
              tick2 = MiWi_TickGet();
              }
  LightTrip = 2;
  
      }      
     }
}
void Check_Node_Type(void)
{
#ifndef MOTORMODULE
   
    switch(rxControlinfo.Node_Type)
            {

                case 0x01:
                    
                   if(rxControlinfo.Node_ON_OFF == 0xAA) 
                   {
                       EEPromData[0] = rxControlinfo.Node_ON_OFF = 0xFF;
                   }
                   else EEPromData[0] = rxControlinfo.Node_ON_OFF = 0xAA;
                    break;               
             
         default:
             break;
     }
#endif     
}

void CheckActionTime (void)
{
    
             if(rxControlinfo.Action_Time_Length)
             {
                 switch(rxControlinfo.Action_Time_Length)
                 {
                                  case 01:

                 TimerOn = 15 ; // 15 seconds
                 tick1 = MiWi_TickGet();
                 break;
                                  case 02:
                                      
                 TimerOn = 30 ; // 15 seconds
                 tick1 = MiWi_TickGet();                        
                 break;
                                  case 03:

                 TimerOn = 60 ; // 60 seconds
                 tick1 = MiWi_TickGet();
                 break;
                                  case 04:

                 TimerOn = 1800 ;// 30 minutes
                 tick1 = MiWi_TickGet();
                 break;
                                  case 05:

                 TimerOn = 3600 ;// 1 hour
                 tick1 = MiWi_TickGet();
                 break;
                                  case 06:

                 TimerOn = 10800 ;//3 hours
                 tick1 = MiWi_TickGet();
                 break;
                 default:
                     break;

                 }

             }else{ TimerOn = 0;}
}

void CompairLightRoutine(void)
{
    if (AmbientLight <= rxControlinfo.Light_Setpoint_Low && rxControlinfo.Light_Action_Low == 1) // Means it's BRIGHT  in the internal sensor
        //(ExternalLightReading <= rxControlinfo.Light_Setpoint_Low) // Means it's Dark in the external sensor
    {
        rxControlinfo.TOO_Bright_Sensor_Trip = 0x01;//opposite dark trip for external sensor
        rxControlinfo.Dark_Sensor_Trip = 00;

    }
    if (AmbientLight >= rxControlinfo.Light_Setpoint_High && rxControlinfo.Light_Action_High == 1)   // Means IT's Time to trip DARK for the internal sensor
       //(ExternalLightReading >= rxControlinfo.Light_Setpoint_High)   // Means IT's Time to trip Too Bright for external sensor
    {
        rxControlinfo.TOO_Bright_Sensor_Trip = 00;
        rxControlinfo.Dark_Sensor_Trip = 01;

    }   
    if  (AmbientLight < rxControlinfo.Light_Setpoint_High && AmbientLight > rxControlinfo.Light_Setpoint_Low && rxControlinfo.Light_Action_High == 1) //Means IT's Normal Brightness
       //(ExternalLightReading < rxControlinfo.Light_Setpoint_High && ExternalLightReading > rxControlinfo.Light_Setpoint_Low) //Means IT's Normal Brightness
    {
        rxControlinfo.TOO_Bright_Sensor_Trip = 00;
        rxControlinfo.Dark_Sensor_Trip = 00;
      
   }
}
void CompairNodeSensorRoutine(void)
{
   if (rxControlinfo.Sensor_Type == temperatureSensor)
   {

    NodeSensorReading = Temperature;
   }
else if (rxControlinfo.Sensor_Type == humiditySensor)
    {

    NodeSensorReading =  RelativeHumidity;
   }

if  (NodeSensorReading < rxControlinfo.Node_Setpoint_Low && rxControlinfo.Node_Action_Low)// like Soil Humidity or water level
    {
        rxControlinfo.Node_Setpoint_Low_Trip = 01;
        rxControlinfo.Node_Setpoint_High_Trip = 00;
    }
       if (NodeSensorReading > rxControlinfo.Node_Setpoint_High && rxControlinfo.Node_Action_High)//like Soil Humidity
    {
        rxControlinfo.Node_Setpoint_Low_Trip = 00;
        rxControlinfo.Node_Setpoint_High_Trip = 01;
    }
       if (NodeSensorReading < rxControlinfo.Node_Setpoint_High  && NodeSensorReading > rxControlinfo.Node_Setpoint_Low && rxControlinfo.Node_Action_High)  // Here we're in between setpoint
    {
        rxControlinfo.Node_Setpoint_Low_Trip = 00;
        rxControlinfo.Node_Setpoint_High_Trip = 00;

    }
}



void ReadingAdjust(unsigned int Sensor)  // the key here is that the 16 bit Hexidecimal WORD now looks like an unsigned int of 16 bits
{
    READING = Sensor;
  Reading[index] = LOWBYTE(READING);   // the data gets split into 2 bytes
  Reading[index+1] = HIGHBYTE(READING);
}

void SendAllRoutine(void) //general routine to send readings
{
   int i;
   index = 0;

   ReadingAdjust(AmbientLight);

   index = 2;
if(rxControlinfo.Sensor_Type == 5){
   ReadingAdjust (Temperature);

}else {
   ReadingAdjust (NodeSensorReading);//(NodeSensorReading);// or soil humidity

}
   index = 4;
#ifdef  DOUBLESENSORMODULE
   ReadingAdjust(RelativeHumidity);

#else
   RelativeHumidity=0;// just for demo without sensor   IMPORTANT,,,JUST FOR CASES OF NO SENSOR HERE
   ReadingAdjust(RelativeHumidity);
#endif
   Reading[6] = LED_2; // this will be the output bit ON or OFF status
   Reading[7] = WindowSetting; // this will be the output either OPEN or CLOSED or 0
   Reading[8] = rxControlinfo.Sensor_Type;
   index = 9;
   ReadingAdjust (rxControlinfo.Node_Setpoint_High);

   index = 11;
   ReadingAdjust (rxControlinfo.Node_Setpoint_Low);

   index = 13;
   ReadingAdjust (rxControlinfo.Light_Setpoint_High);

   index = 15;
   ReadingAdjust (rxControlinfo.Light_Setpoint_Low);
   
   Reading[17] = rxControlinfo.Node_Type;


                    MiApp_FlushTx();

                    for(i = 0; i < 18; i++)
                    {
                        MiApp_WriteData(Reading[i]);
                    }

                   MiApp_BroadcastPacket(FALSE); // Broadcast the info back to the HUB
}

void MotorConrolRoutine ()
{

    switch (rxControlinfo.Motor_Instruction)
    {
        case  0:
            target = 00;
    break;
        case  1:
            target = 1023; // actuator 1/4 out
    break;
        case  2:
            target = 2046;
    break;
        case  3:
            target = 3069;
    break;
        case  4:
            target = 4095;
    break;
        case  5:
            target = (AmbientLight-250)*5.85;
                if(rxControlinfo.Node_Type == 1)
                {
                    target = MaxLength - target;
                }
    break;
    default:
    break;

    }

    if(target > MaxLength){target = MaxLength;} // Maximum extended setting for motor actuator


    serialBytes[0] = 0xAA;
    serialBytes[1] = 11;
    serialBytes[2] = 0xC0 + (target & 0x1F); // Command byte holds the lower 5 bits of target.
    serialBytes[3] = (target >> 5) & 0x7F;   // Data byte holds the upper 7 bits of target.

//ConsolePut(serialBytes[0]);
//ConsolePut(serialBytes[1]);
ConsolePut(serialBytes[2]);
ConsolePut(serialBytes[3]);

}


void DemoOutput_HandleMessage(BYTE TxNum, BYTE RxNum)
{
    BYTE i;
    BYTE *table;
    BYTE a;
    BYTE writeaddr = 0x00;
    BYTE writecount = 9;
    BYTE PayloadIndex;

    if( rxMessage.flags.bits.broadcast == 1 )  // Broadcast from another node is detected
    {
                            switch( rxMessage.PayloadSize )
                            {
                            case 04: // the server has sent the turn ON/OFF command 
                                    if(rxMessage.Payload[1] == 0 )
                                    {
                                EEPromData[0] =  rxControlinfo.Node_ON_OFF = 0xFF;
                                rxControlinfo.Action_Time_Length = actionTimeOld;  // replace the timer value now not in over ride

                                                if (OverRideBit == FALSE)
                                                    {
                                                        OverRideBit = TRUE;
                                                    }else{OverRideBit = FALSE;}
                                Check_OnOFF();
                                    }   
                                    if (rxMessage.Payload[1] == 1 )
                                    {
                                EEPromData[0] =  rxControlinfo.Node_ON_OFF = 0xAA;

                                actionTimeOld = rxControlinfo.Action_Time_Length;
                                rxControlinfo.Action_Time_Length = 0; // clear and hold the timer if on over ride.
                        
                                                 if (OverRideBit == FALSE)
                                                    {
                                                        OverRideBit = TRUE;
                                                    }else{OverRideBit = FALSE;}
                                Check_OnOFF();
                                    }
                                    
                                  break;                                                                 
                            case 0x13:// means the HIGH SET POINT was tripped by the partner device
                               
				EEPromData[0] = rxControlinfo.Node_ON_OFF = 0xAA;
                                Check_Node_Type();
                        Check_OnOFF();
                        CheckActionTime();
                                FlipWriteBit = TRUE;
                                
                                 break;
                            case 0x12:// means a LOW SET POINT was tripped by the partner device
                              
				EEPromData[0] = rxControlinfo.Node_ON_OFF = 0xFF;
                                Check_Node_Type();
                        Check_OnOFF();
                        CheckActionTime();
                                FlipWriteBit = TRUE;
                                
                                break;
                            case 14:// means the DARK SET POINT was tripped by the partner device

				EEPromData[0] = rxControlinfo.Node_ON_OFF = 0xAA;
                                Check_Node_Type();
                        Check_OnOFF();
                        CheckActionTime();
                                FlipWriteBit = TRUE;

                                 break;

                            case 17:// means a BRIGHT SET POINT was tripped by the partner device

				EEPromData[0] = rxControlinfo.Node_ON_OFF = 0xFF;
                                Check_Node_Type();
                        Check_OnOFF();
                        CheckActionTime();
                                FlipWriteBit = TRUE;

                                break;
                                
                            case 21:// means the MOTION was tripped by the partner device

				EEPromData[0] = rxControlinfo.Node_ON_OFF = 0xAA;
                                Check_Node_Type();
                        Check_OnOFF();
                        CheckActionTime();
                                FlipWriteBit = TRUE;

                                 break;
                            default:  // Sensor has sent reading somewhere so...........
 
                            break;
                    }     
    }

    // otherwise it's a unicast from the PAN Coordinator

   if( rxMessage.flags.bits.broadcast == 0 )    //meaning unicast
        {
      switch( rxMessage.PayloadSize )  // 
        {
                                      case 02: // the server has sent the turn ON/OFF command
                                    if(rxMessage.Payload[1] == 0 )
                                    {
                                EEPromData[0] =  rxControlinfo.Node_ON_OFF = 0xFF;
                                rxControlinfo.Action_Time_Length = actionTimeOld;  // replace the timer value now not in over ride

                                                if (OverRideBit == FALSE)
                                                    {
                                                        OverRideBit = TRUE;
                                                    }else{OverRideBit = FALSE;}
                                Check_OnOFF();
                                    }
                                    if (rxMessage.Payload[1] == 1 )
                                    {
                                EEPromData[0] =  rxControlinfo.Node_ON_OFF = 0xAA;

                                actionTimeOld = rxControlinfo.Action_Time_Length;
                                rxControlinfo.Action_Time_Length = 0; // clear and hold the timer if on over ride.

                                                 if (OverRideBit == FALSE)
                                                    {
                                                        OverRideBit = TRUE;
                                                    }else{OverRideBit = FALSE;}
                                Check_OnOFF();
                                    }

                                  break;          
              case 03:// *****************************************************calibrate the motor and set the maximum length of the actuator
                                   for(i = 0; i < rxMessage.PayloadSize; i++)
                    {
                        ReceiveinfoArray[i] = rxMessage.Payload[i];  // Here's where we load the ReceiveinfoArray[] with the payload
                    }
                                   Temp = ReceiveinfoArray[0];
                                   Temp = Temp * 204.75;
                                       
          rxControlinfo.Motor_MaxLength = MaxLength = Temp;
    index = 0;
    ReadingAdjust(MaxLength);
    EEPromData[13] = Reading[0];
    EEPromData[14]= Reading[1];
       FlipWriteBit = TRUE;
                                    break;
              case 04: // accept the new setpoints
                     for(i = 0; i < rxMessage.PayloadSize; i++)
                    {
                        ReceiveinfoArray[i] = rxMessage.Payload[i];  // Here's where we load the ReceiveinfoArray[] with the payload
                    }
                     switch(rxControlinfo.Sensor_Type)

                     {
                         case 0x01:
                             Temp = ReceiveinfoArray[2];  // needs the variable to do the math with - can't do it just
                             Temp = (Temp * 50);                             
                     rxControlinfo.Light_Setpoint_High = Temp;// with the array, won't work
                             Temp = ReceiveinfoArray[3];
                             Temp = (Temp * 50);                      
                     rxControlinfo.Light_Setpoint_Low = Temp;
                             break;
                         case 0x02:
                             Temp = ReceiveinfoArray[0];
                             Temp = (Temp * 25);
                     rxControlinfo.Node_Setpoint_High = Temp;
                             Temp = ReceiveinfoArray[1];
                             Temp = (Temp * 25);
                     rxControlinfo.Node_Setpoint_Low = Temp;
                             break;
                         case 0x03:
                             Temp = ReceiveinfoArray[0];
                             Temp = (Temp * 50);                              
                     rxControlinfo.Node_Setpoint_High = Temp;
                             Temp = ReceiveinfoArray[1];
                             Temp = (Temp * 50);                      
                     rxControlinfo.Node_Setpoint_Low = Temp;
                             break;
                         case 0x04:
                             Temp = ReceiveinfoArray[0];
                             Temp = (Temp * 50); 
                     rxControlinfo.Node_Setpoint_High = Temp;
                             Temp = ReceiveinfoArray[1];
                             Temp = (Temp * 50);                      
                     rxControlinfo.Node_Setpoint_Low = Temp;
                             break;
                         case 0x05:
                             Temp = ReceiveinfoArray[0];
                             Temp = (Temp * 2.5) + 50;
                     rxControlinfo.Node_Setpoint_High = ((ReceiveinfoArray[0] * 2.5) + 50); // turn into temperature setpoints
                             Temp = ReceiveinfoArray[1];
                             Temp = (Temp * 2.5) + 50;
                     rxControlinfo.Node_Setpoint_Low = ((ReceiveinfoArray[1] * 2.5) + 50);
                             break;
                         case 0x06:
                             Temp = ReceiveinfoArray[0];
                             Temp = (Temp * 50);                              
                     rxControlinfo.Node_Setpoint_High = Temp;
                             Temp = ReceiveinfoArray[1];
                             Temp = (Temp * 50);                      
                     rxControlinfo.Node_Setpoint_Low = Temp;
                             break;
                         case 0x07:
                             Temp = ReceiveinfoArray[0];
                             Temp = (Temp * 2.5) + 50;
                     rxControlinfo.Node_Setpoint_High = ((ReceiveinfoArray[0] * 2.5) + 50); // turn into humidity setpoints
                             Temp = ReceiveinfoArray[1];
                             Temp = (Temp * 2.5) + 50;
                     rxControlinfo.Node_Setpoint_Low = ((ReceiveinfoArray[1] * 2.5) + 50);
                             break;
                         default:
                             break;
                     }

                     switch (rxControlinfo.Sensor_Type)
                     {
                         case 00:
                             break;
                         case 01:
                                 index = 0;
    ReadingAdjust( rxControlinfo.Light_Setpoint_High);
    EEPromData[9] = Reading[0];
    EEPromData[10]= Reading[1];
      index = 2;
    ReadingAdjust( rxControlinfo.Light_Setpoint_Low);
    EEPromData[11] = Reading[2];
    EEPromData[12] = Reading[3];
                             break;
                         default:
                                   index = 0;
    ReadingAdjust( rxControlinfo.Node_Setpoint_High );
    EEPromData[9] = Reading[0];
    EEPromData[10] = Reading[1];
     index = 2;
     ReadingAdjust( rxControlinfo.Node_Setpoint_Low);
    EEPromData[11] = Reading[2];
    EEPromData[12]= Reading[3];
                             break;
                     }


              Blink(4); // Blink to indicate connection established
              FlipWriteBit = TRUE;
            
              break;
          case 05: // identify node just blink to say yup
              Blink(5); // Blink to indicate connection established
              FlipWriteBit = FALSE;
              break;

          case 07: // Here we Commission the node to a Room
          PayloadIndex = 4;// pointing to the byte containing the pan id data
                            switch( rxMessage.Payload[0] )
                    {
                        case COMMISSION:
                            {
                                switch( rxMessage.Payload[1] )
                                {
                                    case 0x01:  // flash light

                                            Blink(20); // Blink to indicate connection established

                                        break;

                                    case 0x02:  // set channel and PANID
                                    {
                                            WORD tmp = 0xFFFF;
                                            currentChannel = rxMessage.Payload[PayloadIndex];
                                            MiApp_SetChannel(currentChannel);

                                        	myPANID.v[0] = rxMessage.Payload[PayloadIndex+1];
                                        	myPANID.v[1] = rxMessage.Payload[PayloadIndex+2];
                                            MiMAC_SetAltAddress((BYTE *)&tmp, (BYTE *)&myPANID.Val);
                                        	nvmPutMyPANID(myPANID.v);
                                             
                     
                                                 Blink(20); // Blink to indicate connection established
                                    }
                                        break;
                                    default:
                                        break;
                                }
                          }
                   }
                break;

          case 0x08: // here's where we receive the Node Program data

                for(i = 0; i < rxMessage.PayloadSize; i++)
                    {
                        ReceiveinfoArray[i] = rxMessage.Payload[i];  // Here's where we load the ReceiveinfoArray[] with the payload
                    }
    // then we destribute the payload to the rxControlinfo structure
                rxControlinfo.Node_Type = EEPromData[1] = ReceiveinfoArray[0];  // transfer in Node type

                 if(ReceiveinfoArray[1] == 0) // Meaning just listen to the PAN and ignore the sensors
                    {
                            rxControlinfo.Node_Action_High = EEPromData[3] = 0;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 0;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                            rxControlinfo.Sensor_Type = EEPromData[8] = 0; // NO SENSOR
                    }
                 else if (ReceiveinfoArray[1])  // Meaning there's something in this box
                    {
                        switch (ReceiveinfoArray[2]) // sensor type to be determined
                        {
                            case 00:
                                rxControlinfo.Sensor_Type = EEPromData[8] = 0;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 0;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 0;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                                break;
                            case 01:
                                rxControlinfo.Sensor_Type = EEPromData[8] = lightSensor;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 0;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 0;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 1;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 1;
                                break;
                            case 02:
                                rxControlinfo.Sensor_Type = EEPromData[8] = soilhumiditySensor ;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 1;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 1;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                            rxControlinfo.Node_Setpoint_Low = 100;//
                            rxControlinfo.Node_Setpoint_High = 300;// rough setpoints for soil sensing
                                break;
                            case 03:
                                rxControlinfo.Sensor_Type = EEPromData[8] = waterlevelSensor;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 1;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 1;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                            rxControlinfo.Node_Setpoint_Low = 400;//
                            rxControlinfo.Node_Setpoint_High = 800;//rough water level sensing logic levels translated into HIGH and LOW
                                break;
                            case 04:
                                rxControlinfo.Sensor_Type = EEPromData[8] = switchSensor;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 1;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 1;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                            rxControlinfo.Node_Setpoint_Low = 400;//  
                            rxControlinfo.Node_Setpoint_High = 800;//
                            break;
                            case 05:
                                rxControlinfo.Sensor_Type = EEPromData[8] = temperatureSensor;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 1;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 1;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                            rxControlinfo.Node_Setpoint_Low = 75;//  
                            rxControlinfo.Node_Setpoint_High = 80;//
                            break;
                            case 06:
                                rxControlinfo.Sensor_Type = EEPromData[8] = pressureSensor;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 1;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 1;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                            rxControlinfo.Node_Setpoint_Low = 400;//  
                            rxControlinfo.Node_Setpoint_High = 800;//
                            break;
                            case 07:
                               rxControlinfo.Sensor_Type = EEPromData[8] = humiditySensor;
                            rxControlinfo.Node_Action_High = EEPromData[3] = 1;
                            rxControlinfo.Node_Action_Low = EEPromData[4] = 1;
                            rxControlinfo.Light_Action_High = EEPromData[5] = 0;
                            rxControlinfo.Light_Action_Low = EEPromData[6] = 0;
                            rxControlinfo.Node_Setpoint_Low = 75;//
                            rxControlinfo.Node_Setpoint_High = 90;//
                            break;
                                        
                    default:
                        break;
                        }
                    }
                     switch (rxControlinfo.Sensor_Type)
                     {
                         case 00:
                             break;
                         case 01:
                                 index = 0;
    ReadingAdjust( rxControlinfo.Light_Setpoint_High);
    EEPromData[9] = Reading[0];
    EEPromData[10]= Reading[1];
      index = 2;
    ReadingAdjust( rxControlinfo.Light_Setpoint_Low);
    EEPromData[11] = Reading[2];
    EEPromData[12] = Reading[3];
                             break;
                         default:
                                   index = 0;
    ReadingAdjust( rxControlinfo.Node_Setpoint_High );
    EEPromData[9] = Reading[0];
    EEPromData[10] = Reading[1];
     index = 2;
     ReadingAdjust( rxControlinfo.Node_Setpoint_Low);
    EEPromData[11] = Reading[2];
    EEPromData[12]= Reading[3];
                             break;
                     }
               rxControlinfo.Action_Time_Length = EEPromData[2] = actionTimeOld = ReceiveinfoArray[3]; // transfer in the timer pointer

               rxControlinfo.Motor_Instruction = EEPromData[7] = ReceiveinfoArray[4];
            FlipWriteBit = TRUE;
            
           Check_SetpointsSettings();  // every time you send new programming data, you reset the setpoints to standard
     
             
             Blink(4); // Blink to indicate connection established

            break;

              case 0x09: // send sensor and status
              SendAllRoutine();
              break;
             
          default: 
            break;                
        }
        }    
   }




void ConfigADC_light(void)//light sensor built-in on port RA0
{

    //Enable AN0 as input
    TRISAbits.TRISA0 = 1;

    //Configure the ADC register settings
    ADCON0 = 0x01;  // set up channel 0 as A/D and turn ON the ADC
    ADCON1 = 0x0D;  // set up the A0 and A1 as analog channels
    ADCON2 = 0xB9;  // 20 tad and Fosc/8

    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 0;
}
void ConfigADC_node(void)//Temperature and Soil Humidity on port RA1
{

    //Enable AN1 as input
    TRISAbits.TRISA1 = 1;

    //Configure the ADC register settings
    ADCON0 = 0x05;  // set up channel 1 as A/D and turn ON the ADC
    ADCON1 = 0x0D;  // set up the A0 and A1 as analog channels
    ADCON2 = 0xB9;  // 20 tad and Fosc/8

    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 0;
}
void ConfigADC_node1(void)//Humidity reading on RB1
{

    //Enable AN1 as input
    TRISBbits.TRISB1 = 1;

    //Configure the ADC register settings
    ADCON0 = 0x29;  // set up channel 10 as A/D and turn ON the ADC
    ADCON1 = 0x0D;  // set up the A0 and A1 as analog channels
    ADCON2 = 0xB9;  // 20 tad and Fosc/8

    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 0;
}
void ConfigADC_node2(void)//External light sensor RA3 or other sensor
{

    //Enable AN1 as input
    TRISAbits.TRISA3 = 1;

    //Configure the ADC register settings
    ADCON0 = 0x0D;  // set up channel 3 as A/D and turn ON the ADC
    ADCON1 = 0x0D;  // set up the A0 and A1 as analog channels
    ADCON2 = 0xB9;  // 20 tad and Fosc/8

    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 0;
}




void ReadLightSensor(void)
{
    BYTE    lightArray[samplingRate];
    BYTE    i = 0;

 ConfigADC_light();
     do
    {
        ADCON0bits.ADON = 1; // enable the ADC
        Delay10us(1);       //Acquisition time
        ADCON0bits.GO = 1;
        while(ADCON0bits.DONE);
        {
            WORD    tempValue;
            BYTE    inttemp;
            double  temp;

            tempValue = ADRES;
            if(tempValue  < (AmbientLight - 20))
            { AmbientLight = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion
            if(tempValue  > (AmbientLight + 20))
            { AmbientLight = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion


            //lightArray[i] = (BYTE) temp;// not used
        }

        ADCON0bits.ADON = 0;//disable the ADC
        Delay10us(1);       //A minimum 2TAD is required before the next acquisition starts
        i++;
    } while(i < samplingRate);

}



void ReadNodeSensor(void)  // port RA1  this will read temperature and Soil humidity
{
    BYTE    nodeArray[samplingRate];
    BYTE    i = 0;

 ConfigADC_node();
     do
    {
        ADCON0bits.ADON = 1;
        Delay10us(1);       //Acquisition time
        ADCON0bits.GO = 1;
        while(ADCON0bits.DONE);
        {
            long   tempValue;
            BYTE    inttemp;
            double  temp;

            tempValue = ADRES;
            if(tempValue  < (NodeSensorReading - 20))
            { NodeSensorReading = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion
            if(tempValue  > (NodeSensorReading + 20))
            { NodeSensorReading = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion
           
            Centigrade = tempValue / 10.0;  // temp in Centegrade = Vout (in millivolts) - 500 then devided by 10
           
         /*the Centigrade now holds degrees celcius */

            
             Fahrenheit = ((Centigrade * 9)/5)+32 ;
             Temperature = Fahrenheit ;//Here's the fahreinheit conversion

            
        }

        ADCON0bits.ADON = 0;
        Delay10us(1);       //A minimum 2TAD is required before the next acquisition starts
        i++;
    } while(i < samplingRate);

}
void ReadNode1Sensor(void)  // port RB1 Room Humidity reading
{
    BYTE    nodeArray[samplingRate];
    BYTE    i = 0;
    int volts;
int sensorRH;
int TempsensorRH;
float Tempvolts;
 long trueRH;


 ConfigADC_node1();
     do
    {
        ADCON0bits.ADON = 1;
        Delay10us(1);       //Acquisition time
        ADCON0bits.GO = 1;
        while(ADCON0bits.DONE);
        {
            int    tempValue;
            BYTE    inttemp;
            double  temp;
           float    floatValue;

            tempValue = ADRES;
            
            if(tempValue  < (HumidityReading - 20))
            { HumidityReading = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion
            if(tempValue  > (HumidityReading + 20))
            { HumidityReading = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion
  // convert to voltage value
            volts = (tempValue/2) ;
            Tempvolts = volts/100;
TempsensorRH = 161.0 * Tempvolts;
sensorRH = (TempsensorRH / 5) - 25.8;

 
     
    trueRH = sensorRH / (1.0546 - 0.0026 * Centigrade);//temperature adjustment
    RelativeHumidity = tempValue/10;
        }

        ADCON0bits.ADON = 0;
        Delay10us(1);       //A minimum 2TAD is required before the next acquisition starts
        i++;
    } while(i < samplingRate);

}
void ReadNode2Sensor(void)  // port RA3 External Light sensor
{
    BYTE    nodeArray[samplingRate];
    BYTE    i = 0;

 ConfigADC_node2();
     do
    {
        ADCON0bits.ADON = 1;
        Delay10us(1);       //Acquisition time
        ADCON0bits.GO = 1;
        while(ADCON0bits.DONE);
        {
            WORD    tempValue;
            BYTE    inttemp;
            double  temp;

            tempValue = ADRES;
            if(tempValue  < (ExternalLightReading - 20))
            { ExternalLightReading = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion
            if(tempValue  > (ExternalLightReading + 20))
            { ExternalLightReading = tempValue;  }//actual value in between 0 and 1024 in the 10 bit conversion



            //tempValue = (tempValue - 500.0) / 10.0;

        }

        ADCON0bits.ADON = 0;
        Delay10us(1);       //A minimum 2TAD is required before the next acquisition starts
        i++;
    } while(i < samplingRate);

}






void DemoOutput_UpdateTxRx(BYTE TxNum, BYTE RxNum)
{
    LCDTRXCount(TxNum, RxNum);  
}    

void DemoOutput_ChannelError(BYTE channel)
{
    Printf("\r\nSelection of channel ");
    PrintDec(channel);
    Printf(" is not supported in current configuration.\r\n");
}    

void DemoOutput_UnicastFail(void)
{
    Printf("\r\nUnicast Failed\r\n");                  
    LCDDisplay((char *)" Unicast Failed", 0, TRUE);
}    

